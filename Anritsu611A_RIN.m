





%% File version
% v1.00 (06.03.2019)_AHerdt



%% Header % Comment if you use in a larger script


% instrreset
clear variables
close all
clc

%% Description of measurement


% ### Required Equipment
% ESA
% Current Source
% Multimeter

% ### Possible Determination of
% RIN
% Frequency Noise
% Linewidth via Frequency Noise

% 01. ### Measurement with laser under threshold
% 02. ### Switch on Laser / set it above threshold
% 03. ### Gather ESA-trace and DC-Voltage of detector for each current
% 03.a) # Gather and save ESA-trace for each current
% 03.b) # Gather DC-Voltage of detector for each current
% 03. c)# Safe DC-Voltage Data
% 04. ### Finish (Switch off afterwards)




%% ################### Initialization of devices  ###################


% ################## ESA
% Find a VISA-GPIB object.
obj1 = instrfind('Type', 'visa-gpib', 'RsrcName', 'GPIB0::8::INSTR', 'Tag', '');

% Create the VISA-GPIB object if it does not exist
% otherwise use the object that was found.
if isempty(obj1)
    obj1 = visa('NI', 'GPIB0::8::INSTR');
else
    fclose(obj1);
    obj1 = obj1(1);
end

% Connect to instrument object, obj1.
fopen(obj1);

% Communicating with instrument object, obj1.



% ################## LDC

LDC =  Dev.ILX_LDX3210;
LDC.VisaAdress = 'GPIB0::2::INSTR';
LDC = LDC.DevIni;
LDC.Name = 'LDC ICL2183_13_25';


% ################ LockIn
LockInAmp = Dev.SR530LockIn;
LockInAmp.VisaAdress = 'GPIB0::23::INSTR';
LockInAmp = LockInAmp.DevIni;
LockInAmp.Name = 'LockInAmp ICL2183_13_25';




%% ################## SET DEVICE PARAMETERS ##################

% ################## LASER
Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;




% ################## LASERLIMITS
% Set Laserlimits and so on
LDC.LaserObject = Laser_ICL2183_10_12;
LDC.SLimCurrent = .05;
SafeCurrent = 0.002;

% Set Current Stepsizes
StartCurrent =    0.0250;
EndCurrent   =    0.0250;
StepSizeCurrent = 0.001;

% Set ESA Frequency List
FrequencyList = 1e6:49e6/1000:50e6;

% Insert ESA Sweeptime 
ESASweepTime = .5;


%%  Folder Initialization


FileFolder = 'Measurement1';
MeasurementType = 'RIN';








%% Measurement






% ################## 01. ### Switch on Laser / set it above threshold

LDC.Output = true;
LDC.goSafe2Current(StartCurrent);



% ################## 02. ### Gather ESA Signal for each Frequency and
% current using the lock in



NumSteps = round((EndCurrent - StartCurrent)/StepSizeCurrent + 1);

for i=1:NumSteps;
    % ################## 02.a) # Go To Current
    
    
    NextCurrent =StartCurrent+StepSizeCurrent*(i-1);
    CurrentList(i) = NextCurrent;
    
    disp(['Next Current =' ' ' num2str(NextCurrent*1000) 'mA'])
    
    LDC.SetCurrent = NextCurrent;
    pause(0.2)
    CurrentActList(i) = LDC.ActCurrent;
    
    
    
  
    % ################## 02.b) # Gather LockInSignal for each Frequency
    for j = 1:length(FrequencyList)
        
        pause(LockInAmp.TimeAverage*1.1)
        FrequencySet = FrequencyList(j);
        fprintf(obj1, ['CF' ' ' num2str(FrequencySet)]);
        pauseTime = 1.1*min(LockInAmp.TimeAverage*1.1,ESASweepTime);
        pause(pauseTime)
        DetVoltage(j,i) = LockInAmp.OutputAmp;
        
        % ### Monitoring Plot 1
        figure(1)
        p1 = plot(FrequencyList(1:j)/1e6,DetVoltage(1:j,i),'Color',[0 0.447 0.741],'Linewidth',2);
        drawnow;
        xlabel('Frequency [MHz]')
        ylabel('LockInSignal [V]')
        set(gca,'FontSize',14)
        set(gca,'FontName','Times New Roman')

    end
    
            
        % ### Save Files
        FileName1= [num2str(NextCurrent*1000) 'mA.esatrc'];
        Dev.Util.SafeToAsci([FrequencyList' DetVoltage(:,i)],FileFolder,FileName1,MeasurementType);
    
    FrequencyList2(:,i) = FrequencyList';
    
    
    % ### Monitoring Plot 2
        figure(2)
        p2 = plot(FrequencyList2/1e6,DetVoltage,'Linewidth',2);
        drawnow;
        xlabel('Frequency [MHz]')
        ylabel('LockInSignal [V]')
        set(gca,'FontSize',14)
        set(gca,'FontName','Times New Roman')
        
end






% ################## 04. ### Finish (Switch off afterwards)



LDC.goSafe2Current(SafeCurrent-0.001);
LDC.switchOff();