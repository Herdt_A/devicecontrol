%% Header % Comment if you use in a larger script

instrreset
clear variables
close all
clc

%% Description of measurement


% 01. ### Measurement with laser under threshold
% 02. ### Switch on Laser / set it above threshold
% 03. ### Gather ESA-trace and DC-Voltage of detector for each current
    % 03.a) # Gather and save ESA-trace for each current
    % 03.b) # Gather DC-Voltage of detector for each current
    % 03. c)# Safe DC-Voltage Data
% 04. ### Finish (Switch off afterwards)




%% ################### Initialization of devices & Folders ###################



% ################## ESA
ESA = Dev.RS_FSC6;
ESA.VisaAdress = 'TCPIP0::130.83.3.194::inst0::INSTR';
ESA = ESA.DevIni;
ESA.Name = 'ESA ICL2183_1012';

% ################## FreqGenerator
% FreqGen = Dev.HP8116A;
% FreqGen.VisaAdress = 'GPIB0::16::INSTR';
% FreqGen = FreqGen.DevIni;



% ################## Initialize Folders
FileFolder = 'Measurement3_withoutDetector';
MeasurementType = 'ModulationResponse';





%% ################## SET DEVICE PARAMETERS ##################
for i = 1:100
% ################## Messung
MittenFrequenz = i*0.5; %#MHz
% FreqGen.Frequency = MittenFrequen5z*1e6;


% ################## ESA
ESA.FreqStart = MittenFrequenz*1e6-1.25e6;
ESA.FreqStop =  MittenFrequenz*1e6+1.25e6;
ESA.RBW = 1e4;
ESA.ChangeTO(15);











%% ################## Measurement ##################



% ################## 01. ### Measurement with laser under threshold
ESA = ESA.getTrace;
name = [num2str(MittenFrequenz) 'MHz_25mA'];
p1 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on');
Trace1 = ESA.Trace;
l1 = legend(name);
xlabel('frequency [MHz]')
ylabel('Spectral intensity [dBm]')
set(gca,'FontSize',12)
Dev.Util.SafeEsaTrace(ESA,FileFolder,name,MeasurementType)
max(ESA.Trace(:,2))


% pause(2)


end



% ################## 04. ### Finish (Switch off afterwards)


%% Switch Off / go below threshold
% ESA.RBW = 1e6;
ESA.ContOn;

