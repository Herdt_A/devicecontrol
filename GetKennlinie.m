%% Header % Comment if you use in a larger script

instrreset
clear variables
close all
clc

%% Description of measurement


% 01. ### Measurement with laser under threshold
% 02. ### Switch on Laser / set it above threshold
% 03. ### Gather ESA-trace and DC-Voltage of detector for each current
    % 03.a) # Gather ESA-trace for each current
    % 03.b) # Gather DC-Voltage of detector for each current
% 04. ### Finish (Switch off afterwards)




%% ################### Initialization of devices & Folders ###################





% ################## LDC

LDC =  Dev.ILX_LDX3210;
LDC.VisaAdress = 'GPIB0::2::INSTR';
LDC = LDC.DevIni;
LDC.Name = 'LDC ICL2183_13_25';



% ################ MULTIMETER
MultiM = Dev.Agilent34401A;
MultiM.VisaAdress = 'GPIB0::11::INSTR';
MultiM = MultiM.DevIni;
MultiM.Name = 'Multimeter ICL2183_13_25';




% ################## Initialize Folders
FileFolder = 'Measurement1a';
MeasurementType = 'FrequNoise';





%% ################## SET DEVICE PARAMETERS ##################

% ################## LASER
Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;




% ################## LDC
% Set Laserlimits and so on
LDC.LaserObject = Laser_ICL2183_10_12;
LDC.SLimCurrent = .05;









%% ################## Measurement ##################
if LDC.Output == true;
    LDC.goSafe2Current(0.020);
end





% ################## 02. ### Switch on Laser / set it above threshold

LDC.Output = true;

StartCurrent = 0.001;
LDC.goSafe2Current(StartCurrent);



% ################## 03. ### Gather ESA-trace and DC-Voltage of detector for each current

CurrentList(1) = LDC.ActCurrent;

for i=1:101
    % ################## 03.a) # Gather ESA-trace for each current
    
    
    NexCurrent =StartCurrent+0.00001*(i-1);
    CurrentList(i+1) = NexCurrent;
    disp(['Next Current =' ' ' num2str(NexCurrent*1000) 'mA'])
    LDC.SetCurrent = NexCurrent;
    ESA = ESA.getTrace;
    name = [num2str(LDC.ActCurrent*1000,'%.2f') 'mA'];
    figure(1)
    p1 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on','Linewidth',2);
    l1 = legend(name);
    xlabel('frequency [MHz]')
    ylabel('Spectral intensity [dBm]')
    set(gca,'FontSize',12)
    Dev.Util.SafeEsaTrace(ESA,FileFolder,name,MeasurementType)
    
    
    figure(2)
    p2 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2)-Trace1(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on','Linewidth',2);
    l2 = legend(name);
    xlabel('frequency [MHz]')
    ylabel('Spectral intensity - Ref[dBm]')
    set(gca,'FontSize',12)
    
    % ################## 03.b) # Gather DC-Voltage of detector for each current
    DetVoltage(i+1) = MultiM.VoltageDC;
    figure(3)
    if i>2
    p3 = plot(CurrentList(2:end)*1000,DetVoltage(2:end),'Color',[0 0.447 0.741],'LineSmoothing','on','Linewidth',2);
    l3 = legend(name);
    xlabel('current [mA]')
    ylabel('detector voltage[dBm]')
    set(gca,'FontSize',12)
    end
    pause(1)
end


dlmwrite('Traceabc.DCVolt',[CurrentList' DetVoltage']);

% ################## 04. ### Finish (Switch off afterwards)


%% Switch Off / go below threshold
ESA.RBW = 1e6;
ESA.ContOn;
LDC.goSafe2Current(0.025);

