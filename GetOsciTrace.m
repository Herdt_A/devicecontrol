%% Header % Comment if you use in a larger script

instrreset 
clear variables
close all
clc

%% ################### Initialization
A =  Dev.DPO3032;
A.VisaAdress = 'USB0::0x0699::0x0412::C010898::0::INSTR';
A.VisaType = 'visa-usb';
A = A.DevIni;



LDC =  Dev.ILX_LDX3210;
LDC.VisaAdress = 'GPIB0::2::INSTR';
LDC = LDC.DevIni;
LDC.Name = 'LDC ICL2183_13_25';




%% ################## SET DEVICE PARAMETERS ##################

% ################## LASER
Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;




% ################## LASERLIMITS
% Set Laserlimits and so on
LDC.LaserObject = Laser_ICL2183_10_12;
LDC.SLimCurrent = .05;
SafeCurrent = 0.002;

% Set Current Stepsizes
StartCurrent =    0.0200;
EndCurrent   =    0.0350;
StepSizeCurrent = 0.0005;







% ################## 01. ### Switch on Laser / set it above threshold

LDC.Output = true;
LDC.goSafe2Current(StartCurrent);



% ################## 02. ### Gather ESA Signal for each Frequency and
% current using the lock in



NumSteps = round((EndCurrent - StartCurrent)/StepSizeCurrent + 1);

for i=1:NumSteps;
    % ################## 02.a) # Go To Current
    
    
    NextCurrent =StartCurrent+StepSizeCurrent*(i-1);
    CurrentList(i) = NextCurrent;
    
    disp(['Next Current =' ' ' num2str(NextCurrent*1000) 'mA'])
    
    LDC.SetCurrent = goSafe2Current(NextCurrent);
    pause(0.2)
    CurrentActList(i) = LDC.ActCurrent;

A = A.getTrace;
p1 = plot(A.Trace(:,1),A.Trace(:,2),'Color',[0 0.447 0.741 .2],'LineSmoothing','on');
Trace1{i} = A.Trace;
hold on


end

