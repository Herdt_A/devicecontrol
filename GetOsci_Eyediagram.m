%% Header % Comment if you use in a larger script

instrreset
clear variables
close all
clc



%% ################### Settings


% Folder and FileName for Data Saving
SaveFileFolder = 'E:\Nextcloud\Measurement\Olivier\2019_04_16_(Chaos_Message)';
SaveFileType = '*.oscitrc';
SaveFileName = 'tek0000ALL';
MeasurementName = 'Measurement1';

% Number of Points for 1 Eyediagram;
Clock_num = 1;
Points = 5e3;

% Normalized threshold for retrieved message;
MsgThreshold = 0.45;

%% ################### Initialization
Osci =  Dev.DPO3032;
Osci.VisaAdress = 'USB0::0x0699::0x0412::C010898::0::INSTR';
Osci.VisaType = 'visa-usb';
Osci = Osci.DevIni;

load('E:\Nextcloud\Measurement\Olivier\Filter.mat');


%% ################## Get Traces ##################


% CH1_Trace(:,1) = linspace(0,1e4,1e4);
% CH1_Trace(:,2) = rand(1e4,1);
% CH2_Trace = rand(1e4,2);
% MSG_Trace = rand(1e4,2);


Osci.Channel = 'CH1';
Osci = Osci.getTrace;
CH1_Trace = Osci.Trace;

Osci.Channel = 'CH2';
Osci = Osci.getTrace;
CH2_Trace = Osci.Trace;
CH2_Save = CH2_Trace(:,2);
MSG_Save = MSG_Trace(:,2);

Osci.Channel = 'REF1';
Osci = Osci.getTrace;
MSG_Trace = Osci.Trace;

p1 = plot(CH1_Trace(:,1),CH1_Trace(:,2),CH2_Trace(:,1),CH2_Trace(:,2),MSG_Trace(:,1),MSG_Trace(:,2));
p1(1).Color = [0 0 1 .2];
p1(2).Color = [1 0 0 .2];
p1(2).Color = [0 1 0 .2];


l1 = legend('Slave Laser','Master Laser','Message');
l1.Location = 'northoutside';
l1.Orientation = 'horizontal';
drawnow();

%% ################## Evaluate Timetraces ##################






% Parfor Loop, because saving takes long and calculating takes long




clc
disp('Moving Traces to GPU')
% Move to GPU, because Speed increase = Factor 8!
CH1_GPU = gpuArray(CH1_Trace(:,2));
CH2_GPU = gpuArray(CH2_Trace(:,2));
MSG_GPU = gpuArray(MSG_Trace(:,2));




clc
disp('Filtering Timetraces')
% Filter Timetraces
CH1_GPU = filter(Filter,CH1_GPU);
disp('Done for CH1')
CH2_GPU = filter(Filter,CH2_GPU);
disp('Done for CH2')
MSG_GPU = filter(Filter,MSG_GPU);
disp('Done for REF')




clc
disp('Normalizing Timetraces')
% Normalize Timetraces
CH2_GPU = CH2_GPU/max(CH2_GPU);
CH1_GPU = CH1_GPU/max(CH1_GPU);
MSG_GPU = MSG_GPU/max(MSG_GPU);



% Calculate Differnce
DiffCHsre = CH2_GPU-CH1_GPU;



clc
disp('Shifting Timetraces')
% Shift the Message to make the message match
[int,lag] = xcorr(MSG_GPU,DiffCHsre);
Shifter = lag(find(int == max(int)));
MSG_GPU = circshift(MSG_GPU,-Shifter);




clc
disp('Reshaping Timetraces')
% Reshape to Plot Eyediagram
CH2_GPUre = reshape(CH2_GPU,Points*Clock_num,length(CH2_GPU)/(Points*Clock_num));
CH1_GPUre = reshape(CH1_GPU,Points*Clock_num,length(CH1_GPU)/(Points*Clock_num));
MSG_GPUre = reshape(MSG_GPU,Points*Clock_num,length(CH1_GPU)/(Points*Clock_num));
DiffCHsre = reshape(DiffCHsre,Points*Clock_num,length(CH1_GPU)/(Points*Clock_num));



% Eye Diagram for Master
clc
disp('Plotting Diagram 1')
figure(1)
for j = 1:size(CH2_GPUre,2)
    plot(CH2_GPUre(:,j),'Color',[0 0.4470 0.7410 0.3],'Linewidth',2)
    hold on
end
title(['Eye Diagram of Master'])
xlabel('\textbf{Osci Points}','Interpreter','Latex');
ylabel('\textbf{Normalized Intensity}','Interpreter','Latex')
set(gca,'FontName','Times New Roman')
set(gca,'FontSize',14)
ylim([-1 1])
hold off




% Eye Diagram for Slave
pause(.2)
clc
disp('Plotting Diagram 2')
figure(2)
for j = 1:size(CH1_GPUre,2)
    plot(CH1_GPUre(:,j),'Color',[0 0.4470 0.7410 0.3],'Linewidth',2)
    hold on
end
title(['Eye Diagram of Slave'])
xlabel('\textbf{Osci Points}','Interpreter','Latex');
ylabel('\textbf{Normalized Intensity}','Interpreter','Latex')
set(gca,'FontName','Times New Roman')
set(gca,'FontSize',14)
ylim([-1 1])
hold off


% Eye Diagram for Difference
pause(.2)
clc
disp('Plotting Diagram 3')
figure(3)
for j = 1:size(DiffCHsre,2)
    plot(DiffCHsre(:,j),'Color',[0 0.4470 0.7410 0.3],'Linewidth',2)
    hold on
end
title(['Eye Diagram of Master-Slave'])
xlabel('\textbf{Osci Points}','Interpreter','Latex');
ylabel('\textbf{Normalized Intensity}','Interpreter','Latex')
set(gca,'FontName','Times New Roman')
set(gca,'FontSize',14)
hold off
ylim([-1 1])




% Eye Diagram for Real Message
clc
disp('Plotting Diagram 4')
figure(4)
for j = 1:size(MSG_GPUre,2)
    plot(MSG_GPUre(:,j),'Color',[0 0.4470 0.7410 0.3],'Linewidth',2)
    hold on
end
ylim([-1 1])
title(['Eye Diagram of F Message'])
xlabel('\textbf{Osci Points}','Interpreter','Latex');
ylabel('\textbf{Normalized Intensity}','Interpreter','Latex')
set(gca,'FontName','Times New Roman')
set(gca,'FontSize',14)
hold off








clc
disp('Calculating Bit Errors')
% Calculate transferred and received Bits;
DiffMsg_2 = DiffCHsre > MsgThreshold;
FilteMsg_2 = MSG_GPUre > MsgThreshold;
Mastr_re_2 = CH2_GPUre > MsgThreshold;
Slave_re_2 = CH1_GPUre > MsgThreshold;

% Look for a trigger in each column
% If you have at least 1 or more trigger points the Bit is considered as 1,
% if you have 0, the Bit is considered as 0
TrsMsg = sum(FilteMsg_2,1)> 0;
RecMsg = sum(DiffMsg_2,1) > 0;
RecMastr = sum(Mastr_re_2,1) > 0;
RecSlave = sum(Slave_re_2,1) > 0;

% Plot Results
figure(5)
hold on
plot(TrsMsg)
plot(RecMsg-1)
plot(RecMastr-2)
plot(RecSlave-3)
hold off

% Calculate Number of Errors
ErrorRec = sum((TrsMsg + RecMsg) == 1);
ErrorMastr = sum((TrsMsg + RecMastr) == 1);
ErrorSlave = sum((TrsMsg + RecSlave) == 1);


L2 = ['Received from Difference with' ' ' num2str(ErrorRec) ' ' 'errors'];
L3 = ['Received from Master with' ' ' num2str(ErrorMastr) ' ' 'errors'];
L4 = ['Received from Slave with' ' ' num2str(ErrorSlave) ' ' 'errors'];

l1 = legend('Transferred',L2,L3,L4);


% Save OsciTrace to Folder
clc
disp('Saving...')
FileFound = dir([SaveFileFolder '\' MeasurementName '\' SaveFileType]);
if ~isempty(FileFound)
    LastFileFound = FileFound(end).name;
    newnumber = str2num(LastFileFound(4:7))+1;
    FileName2 = ['tek' num2str(newnumber,'%04.0f') 'ALL'];
else
    FileName2 = SaveFileName;
end

SaveData = [CH1_Trace CH2_Save MSG_Save];
%         Dev.Util.SafeToAsci_extern(SaveData,SaveFileFolder,FileName2,',');
Dev.Util.SafeOsciTrace(SaveData,MeasurementName,FileName2,SaveFileFolder);
disp('Saving... Done!')

