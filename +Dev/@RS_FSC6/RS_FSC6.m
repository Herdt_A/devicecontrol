classdef RS_FSC6 < Dev.SuperDev.Oscilloscope
 % Maximum 
    
    
    
    properties
        
        %VISA PROPERTIES
        AvailableChannels = {'CH1'};
        Channel = 'CH1'
        FreqStart
        FreqStop
        RBW
        VBW
        
        %LDC specific PROPERTIES
        Trace
        Name        = 'ESA 1'
        %         ConnectedTEC
        
    end
    
    
    
    
    properties (Access = private)
        SweepPoints = 631;
    end
    
    
    
    
    
    
    
    
    
    %     %% Private methods
    methods
        
        function ClearErr(obj)
                        VisaWrite(obj,'*CLS')
                        
        end
        
        function obj = RS_FSC6(obj)
            obj.VisaType = 'visa-tcpip';
        end
        
        function obj = set.Channel(obj,Value)
            % Set the slot
            
            
            % Check input
            Flag1 = ischar(Value); % It must be an string
            
            Flag2 = any(strcmp(Value,obj.AvailableChannels));
            if and(Flag1,Flag2)
                obj.Channel = Value; % Set the value
            else
                error('Channel must be string of all available Channels')
            end
            
        end
        
        
        function Answer = get.FreqStop(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'SENS:FREQ:STOP?';
            Answer = VisaRead(obj,Command);
            Answer = str2num(Answer);
        end
        
        function Answer = get.FreqStart(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'SENS:FREQ:STAR?';
            Answer = VisaRead(obj,Command);
            Answer = str2num(Answer);
        end
        
        function Answer = get.RBW(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the bandwidth.
            %             StrValue = obj.Channel;
            Command = 'BAND:RES?';
            Answer = VisaRead(obj,Command);
            Answer = str2num(Answer);
        end
        
        
        function Answer = get.VBW(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the video bandwidth.
            %             StrValue = obj.Channel;
            Command = 'BAND:VID?';
            Answer = VisaRead(obj,Command);
            Answer = str2num(Answer);
        end
        
        
        
        
        function Answer = errorCheck(obj)
%              
            Answer{1} = VisaRead(obj,'*STB?');
            Answer{2} = VisaRead(obj,'SYST:ERR?');
            
        end
        
        function obj = getTrace(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
%             Time2 = str2num(VisaRead(obj,'SWE:TIME?;))
            obj.ClearErr();

            Time2 = str2double(VisaRead(obj,'SWE:TIME?'));
    
            VisaWrite(obj,':INIT:CONT OFF')
            VisaRead(obj,'*OPC?');
            ans1 = errorCheck(obj);


            
            VisaWrite(obj,'INIT')
            VisaRead(obj,'*OPC?');
           
            Command = 'TRAC:DATA? TRACE1';
            VisaWrite(obj,Command);

            
%             Adjust Input Buffer Size
            fclose(obj.VisaInterfaceObject);
            obj.VisaInterfaceObject.InputBufferSize =  8192;
            obj.VisaInterfaceObject.OutputBufferSize = 8192;
            fopen(obj.VisaInterfaceObject);

            
            % Initialization for Read Out Loop
            StopIteration = false;
            i =0;
            Data = [];
            Show = false;
%             VisaWrite(obj,'CURVE?');
            
            %             Show = true;
            
            if Show
                f = waitbar(0,[obj.Name ': Read Out:' num2str(0) '%']);
            end
            
            while ~StopIteration


                %                 pause(.1)
                i = i+1;
                if Show
                    waitbar(i/NumberOfIterations,f,[obj.Name ': Read Out:' num2str(round(i/NumberOfIterations*100)) '%'])
                end
                %                 VisaRead{'BUSY?'}
                DataArray{i} = fread(obj.VisaInterfaceObject, floor(obj.VisaInterfaceObject.InputBufferSize) , 'uint8');
                length(DataArray{i});
                %                 pause(0.1)
                StopIteration = length(DataArray{i}) < floor(obj.VisaInterfaceObject.InputBufferSize);
                Data = [Data;DataArray{i}];
%                 pause(.1)
            end
            
             if cast(Data(1),'char')=='#'
                CheckBinary = true;
                CheckNumber = str2num(cast(Data(2),'char'));
                ByteNumber =  str2num(cast(Data(3:3+CheckNumber-1),'char')');
            else
                error('Data not in binary mode')
             end
            
            Answer(:,1) = linspace(obj.FreqStart,obj.FreqStop,obj.SweepPoints);
%             size(typecast(typecast(cast(Data(3+CheckNumber:end-1),'uint8'),'uint32'),'single'))
            Answer(:,2) = typecast(typecast(cast(Data(3+CheckNumber:end-1),'uint8'),'uint32'),'single');
%             size(Answer)
            obj.Trace = Answer;
            ans3 = obj.errorCheck();
            VisaRead(obj,'*OPC?');
            
%             for i=1:631
%                 Sign = num2str(Answer(i,1));
%                 Exponent = num2str(bin2dec(Answer(i,2:9)));
%                 Fraction = 0;
%                 for j=1:20
%                 Fraction = Fraction + 2^-j *num2str(Answer(i,j));
%                 end
%                 Value(i) = (-1)^Sign* (1+Fraction)*2^(Exponent-127);
%             end

%                  hex = '0123456789abcdef'; %Hex characters
%                  bins = reshape(Answer,4,numel(Answer)/4).'; %Reshape into 4x(L/4) character array
%                 nums = bin2dec(bins); %Convert to numbers in range of (0-15)
%                 hc = hex(nums + 1); %Convert to hex characters
%                 f = hex2num(hc); %Convert from hex to float
% 
% %             Answer(:,2) = (Answer(:,3) - yoff)*ymult + yzero;
% %             Answer(:,1) =  (0:xincr:xincr*(length(Answer)-1))';
% %             varargout = headerdata';
%             obj.Trace = Answer;
            
            
        end
        
                
                         function obj = set.FreqStart(obj,Value)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            if Value < 9e3;
                Value = 9e3;
            end
            PreCommand = 'FREQ:STAR';
            Command = [PreCommand ' ' num2str(Value)];
            VisaWrite(obj,Command);          
        end
                
                 function obj = set.FreqStop(obj,Value)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            PreCommand = 'FREQ:STOP';
            Command = [PreCommand ' ' num2str(Value)];
            VisaWrite(obj,Command);          
        end
%         

                 function obj = set.RBW(obj,Value)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            PreCommand = 'BAND:RES';
            Command = [PreCommand ' ' num2str(Value)];
            VisaWrite(obj,Command);          
                 end
        
                 
                 function ContOn(obj)
                                 Command = ':INIT:CONT ON';
           
            VisaWrite(obj,Command); 
                 end
                 
                 function ChangeTO(obj,Value)
                     obj.VisaInterfaceObject.Timeout=Value;
                 end
%         function obj = get.FreqStart(obj,Value)
%             % This function sends a command to the DPO 3032 device in order
%             % to choose the right trace. It is necessary to include this
%             % function before every set or get command to assure that it
%             % gets data from the correct Channel.
%             %             StrValue = obj.Channel;
%             Command = 'SENS:FREQ:STAR?';
%             Answer = VisaRead(obj,Command);
%             Answer = str2num(Answer);
%         end
%         
%         function obj = get.RBW(obj,Value)
%             % This function sends a command to the DPO 3032 device in order
%             % to choose the right trace. It is necessary to include this
%             % function before every set or get command to assure that it
%             % gets data from the correct Channel.
%             %             StrValue = obj.Channel;
%             Command = 'BAND:RES?';
%             Answer = VisaRead(obj,Command);
%             Answer = str2num(Answer);
%         end
%         
%         
%         function obj = get.VBW(obj,Value)
%             % This function sends a command to the DPO 3032 device in order
%             % to choose the right trace. It is necessary to include this
%             % function before every set or get command to assure that it
%             % gets data from the correct Channel.
%             %             StrValue = obj.Channel;
%             Command = 'BAND:VID?';
%             Answer = VisaRead(obj,Command);
%             Answer = str2num(Answer);
%         end
        
        
        
    end
end







