classdef VisaObject
    % VisaObject is a superclass for all laboratory devices using a Visa Connection via NI-Visa Driver
    % PROPERTIES:
    % VisaAdress (string)
    % Type (string - USB or GPIB or ETH (Ethernet))
    % Status (bool - true (connected) false (unconnected)) default = false;
    % VisaInterfaceObject (visaObject)
    
    properties
        VisaAdress              % Actual Adress
        VisaType                    % USB or GPIB
        VisaStatus = false;         % Connection Status: true (connected) false (unconnected))
        VisaInterfaceObject     % Actual Visa Object
        
    end
    
    
    %% SET METHODS
    methods
        
        
        function obj = set.VisaAdress(obj,Value)
            % Set the desired VisaAdress of the devices VisaObject
            % input must be a string
            
            % Check input
            if ~ischar(Value)
                error('VisaAdress Input must be a string')
            end
            obj.VisaAdress = Value;
        end
        
        function obj = set.VisaType(obj,Value)
            % Set the desired Tpye of the devices VisaObject
            % input must be a string (USB or GPIB)
            
            % Check input
            if ~ischar(Value)
                error('Type Input must be a string (USB, ETH or GPIB)')
            elseif ~or(or(strcmp(Value,'visa-usb'),strcmp(Value,'visa-gpib')),strcmp(Value,'visa-tcpip'))
                error('Type Input must be a visa-usb or visa-gpib or visa-eth')
            end
            obj.VisaType = Value;
        end
        
        function obj = set.VisaStatus(obj,Value)
            % Set the desired Status of the devices VisaObject
            % input must be a boolean: true (connected) false (disconnected)
            
            % Check input
            if ~islogical(Value)
                error('Status Input must be true (connected) or false (disconnected)')
            end
            obj.VisaStatus = Value;
        end
        
        
        function obj = set.VisaInterfaceObject(obj,VisaInterfaceObject_input)
            % Set the desired Current of the devices VisaObject
            
            % Check input
            if ~or(isa(VisaInterfaceObject_input,'visa'),isa(VisaInterfaceObject_input,'instrument')) % input must be a float
                error('VisaInterfaceObject Input must be of type visa.')
            end
            obj.VisaInterfaceObject = VisaInterfaceObject_input;
        end
        
        %% ALL OTHER METHODS are found in the folder
        
%         function obj = Initialize(obj)
%             obj = Initialize(obj);
%         end
% 
% function obj = VisaWrite(obj)
%     obj = Vis
%         
    end
    
end

