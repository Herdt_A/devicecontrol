function obj = DevIni(obj)
% Open Communication channel if device has not already been
% initialized





if isempty(instrfind('Type',obj.VisaType, 'RsrcName', obj.VisaAdress, 'Tag', ''))   
    
    visa('ni',obj.VisaAdress);   % If no visa Object exist create it
    obj.VisaInterfaceObject = instrfind('Type', obj.VisaType, 'RsrcName', obj.VisaAdress, 'Tag', ''); % Search for it instrument, find it and give it to the object
    
    fopen(obj.VisaInterfaceObject); % Open Communication channel
    Dev.Util.cprintf('[0,.5,0]',['Device with adress' ' ' '"' obj.VisaAdress '"' ' ' 'successfully connected\n']) % Display success
    
    
elseif length(instrfind('Type', obj.VisaType, 'RsrcName', obj.VisaAdress, 'Tag', ''))>1 % Create error message if 
    error('VisaAdress double use')


else
    warning(['Device with adress' ' ' '"' obj.VisaAdress '"' ' ' 'already connected'])
    obj.VisaInterfaceObject = instrfind('Type', obj.VisaType, 'RsrcName', obj.VisaAdress, 'Tag', '');
    
    if strcmp(obj.VisaInterfaceObject.Status,'closed')
        fopen(obj.VisaInterfaceObject);
    end

    
end

obj.VisaStatus = true;

end