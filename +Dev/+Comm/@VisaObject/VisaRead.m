        function [DeviceAnswerRaw,varargout] = VisaRead(obj,Message)
            % Check input - all visa messages are of type char
            if ~ischar(Message)
                error('message has to be of type char')
            end
            
            
            % Communication with device. Device returns raw message
            % including the sent message without "?".
            DeviceAnswerRaw = query(obj.VisaInterfaceObject,Message); 
            
            % Remove sent message
            SearchString = strrep(Message,'?',' '); % Replace "?" of original message by blanc space
            idx = strfind(DeviceAnswerRaw,SearchString); % find the original message (with blanc) in the raw message of the device.
            StringStart = idx + length(SearchString); % get the index of it
            
            if isempty(StringStart)
                StringStart = 1;
            end
            
            DeviceAnswer = DeviceAnswerRaw(StringStart:end); % Create message variable which contains device answer without original message
                  
            varargout{1} = DeviceAnswer; % Device Answer without original message

            
        end