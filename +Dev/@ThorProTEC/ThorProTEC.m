classdef ThorProTEC < Dev.SuperDev.ThorPro800 & Dev.SuperDev.TEC
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        
        
        HLimCurrent   % Hardware limit of current
        
        
        CommandsList = ...
            {':VTE:ACT?',...
            ':ITE:ACT?',...
            ':LIMT:SET?',...
            ':TEC?',...
            ':TEMP:SET?',...
            ':TEMP:ACT?',...
            ':TEMP:SET',...
            ':TEC'}
        
        VariableList = ...
            {'gVoltage',...
            'gPeltCurrent',...
            'gSLimCurrent',...
            'gOutput',...
            'gSetTemp',...
            'gActTemp',...
            'sSetTemp',...
            'sOutput'}
        
        
    end
    
    
    
    
    methods
        
        function Answer = get.HLimCurrent(obj)
            if ~strcmp(obj.ThorType,'TEC')
                error('No TEC chosen - consider other ThorPro Slot.')
            end
            obj.SlotIni; % needed to choose correct slot
            Command = ':LIMTP:ACT?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
    end
    
end


