classdef Vertex
    %VERTEX Summary of this class goes here
    %   Detailed explanation goes here    
    properties
        URI = 'http://130.83.3.94/brow_cmd.htm?';
        DIS=1; %Dummy 
        CNM='Default'; %OperatorName
        REP=1; %Number of Repetitions 
        SFM='Sample_Form';
        SNM='Sample_Name'; 
        DEL=0; % Delay
        NSS=1; % Number of scans
        RES=1; % Resolution
        OPF=1; % Optical filter, 1 = none
        SRC=200; % Source, 200 = Right Input
        APT=1000; % Optical Aperture 1000 = 1mm
        CHN=1; % Channel, Sample Compartment
        DTC=16449; % Detector Setting, 16449 = MCT_DC
        VEL=20000; % 20kHZ, Mirror
        BMS=1; % Beamsplitter
        DLY=0; % Stabilization Delay
        GNS=1; % Signal Gain Multiplier
        SON=0; % External Trigger
        LPF=10000; % Low Pass Filter Frequency 5-40kHz or Open (80kHz)
        TAM=1; % True Dynamic alignment mode
        AQM='SN'; % Aquisition Mode Double sided
        COR=0; % Correlation
        HFW=15790.460824; % High Frequency Limit in cm^-1
        LFW=10.000000; % Low Frequqncy Limit in cm^-1
        PHR=8; % Phase Resolution, must be 8*RES
        SOT=0; % No Idea
    end
    
    methods
        function obj = Vertex(RES ,NSS, HFW, LFW)
            %VERTEX Construct an instance of this class
            %   Detailed explanation goes here
            obj.RES = RES;
            obj.NSS = NSS;
            obj.HFW = HFW;
            obj.LFW = LFW;
            obj.PHR = 8*RES;
        end
               
        function obj = setDetector_MCT_DC(obj)
            obj.DTC = 16449;
        end
        
        function obj = setDetector_MCT_AC(obj)
            obj.DTC = 16448;
        end
        
        function obj = setDetector_InGaAs_AC(obj)
            obj.DTC = 16704;
        end
        
        function obj = setDetector_InGaAs_DC(obj)
            obj.DTC = 16705;
        end
        
        function obj = setDetector_Bolo(obj)
            obj.DTC = 17152;
        end
        
        function file = startMeasurement(obj, path_to_file)         
            req = matlab.net.http.RequestMessage; % Generic HTTP GET Request
            starterror = send(req,'http://130.83.3.94/brow_diag.htm');
            starterr = isempty(strfind(starterror.Body.Data, 'ERROR'));
            if(starterr)
            url = obj.generateHTTPRequest; 
            answ = send(req,url); % HTTP Response
            if(answ.StatusCode ~= 'OK') % 200 = OK 
                error('HTTP Error, Status Code not OK')
            end
            answ = send(req,'http://130.83.3.94/brow_stat.htm');
            counter = 0;
            empterr = 1;
            % look for busy in the html body, if it is busy, do while loop
            while( (isempty(strfind(answ.Body.Data, 'Idle')) && counter < 150) && empterr) % Wait 15 Minutes max
                pause(6) % Give the vertex some time to get data
                answ = send(req,'http://130.83.3.94/brow_stat.htm');
                errorflag = send(req,'http://130.83.3.94/brow_diag.htm');
                empterr = isempty(strfind(errorflag.Body.Data, 'ERROR'));
                counter = counter + 1;
            end
            if(~empterr)
                pause(10)
                startMeasurement(obj, path_to_file);
            else
            file_raw = convertStringsToChars(answ.Body.Data);
            filename = regexp(file_raw,'([A-Za-z])+([0-9])+([\.0])+','once','match'); 
            %Find Filename beginning with upper or 
            %lower case character followed by numbers and terminated .0
            file = websave(path_to_file,['http://130.83.3.94/' filename]);
            end
            else
                pause(10)
                startMeasurement(obj,path_to_file);
            end
        end
        
        function uri = generateHTTPRequest(obj)
            str = [obj.URI,'DIS=' num2str(obj.DIS)];
            str = [str '&CNM=' obj.CNM];
            str = [str '&REP=' num2str(obj.REP)];
            str = [str '&SFM=' obj.SFM];
            str = [str '&SNM=' obj.SNM];
            str = [str '&DEL=' num2str(obj.DEL)];
            str = [str '&NSS=' num2str(obj.NSS)];
            str = [str '&RES=' num2str(obj.RES)];
            str = [str '&OPF=' num2str(obj.OPF)];
            str = [str '&SRC=' num2str(obj.SRC)];
            str = [str '&APT=' num2str(obj.APT)];
            str = [str '&CHN=' num2str(obj.CHN)];
            str = [str '&DTC=' num2str(obj.DTC)];
            str = [str '&VEL=' num2str(obj.VEL)];
            str = [str '&BMS=' num2str(obj.BMS)];
            str = [str '&DLY=' num2str(obj.DLY)];
            str = [str '&GNS=' num2str(obj.GNS)];
            str = [str '&SON=' num2str(obj.SON)];
            str = [str '&LPF=' num2str(obj.LPF)];
            str = [str '&TAM=' num2str(obj.TAM)];
            str = [str '&AQM=' obj.AQM];
            str = [str '&COR=' num2str(obj.COR)];
            str = [str '&HFW=' num2str(obj.HFW)];
            str = [str '&LFW=' num2str(obj.LFW)];
            str = [str '&PHR=' num2str(obj.PHR)];
            str = [str '&SOT=' num2str(obj.SOT)];
            uri = [str '&WRK=' 'Start+Measurement'];
        end
    end
end

