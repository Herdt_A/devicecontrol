function [obj,varargout] = getTrace(obj,StartPoint,StopPoint)
            obj.ChannelIni;
            obj.OutputFormatIni;

            StrValue = num2str(StartPoint);
            PreCommand = 'DAT:STAR';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
            
            
            StrValue = num2str(StopPoint);
            PreCommand = 'DAT:STOP';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
            
            VisaRead(obj,':WFMO:BYT_N?');
            VisaRead(obj,':DAT:ENC?');
            
            VisaRead(obj,':DAT:ENC?');
            
%             VisaRead(obj,'*WAI')
            yoff = str2num(VisaRead(obj,':WFMPRE:YOFF?'));
            ymult = str2num(VisaRead(obj,':WFMPRE:YMULT?'));
            yzero = str2num(VisaRead(obj,':WFMPRE:YZERO?'));
            xincr = str2num(VisaRead(obj,':WFMPRE:XINCR?'));
            xzero = str2num(VisaRead(obj,':WFMPRE:XZERO?'));
            wid = VisaRead(obj,':DAT:WID?');
            
            
            readasync(obj.VisaObject)
%             Value1 = obj.VisaObject.InputBufferSize;
%             Value2 = obj.VisaObject.OutputBufferSize;


            fclose(obj.VisaObject)
            obj.VisaObject.InputBufferSize = 4096;
            obj.VisaObject.OutputBufferSize = 4096;
            fopen(obj.VisaObject)
            
            pause(.01)
            
            StopIteration = false;
            i =0;
            Data = [];
            VisaWrite(obj,'CURVE?')
            
            
            while ~StopIteration
                i = i+1;
                %                 VisaRead{'BUSY?'}
                [DataArray{i},count,msg] = fread(obj.VisaObject, floor(obj.VisaObject.InputBufferSize) , 'uint8');
                length(DataArray{i});
                %                 pause(0.1)
                msg;
                StopIteration = length(DataArray{i}) < floor(obj.VisaObject.InputBufferSize);
                Data = [Data;DataArray{i}];
            end
            
            
            
            if cast(Data(1),'char')=='#'
                CheckBinary = true;
                CheckNumber = str2num(cast(Data(2),'char'));
                ByteNumber =  str2num(cast(Data(3:3+CheckNumber-1),'char')');
            else
                error('Data not in binary mode')
            end
            
            
           headerdata =  cast(Data(1:3+CheckNumber-1),'char');
            
%             Answer{5} = DataArray;
            Answer(:,3) = cast(swapbytes(typecast(cast(Data(3+CheckNumber:end-1),'uint8'),'int16')),'double');
%             Answer(:,3) = swapbytes(typecast(cast(Data(3+CheckNumber:end-1),'uint8'),'uint16'));
%             Answer(1:100000,4) = typecast(cast(Data(3+CheckNumber:end-1),'uint8'),'uint16');
            Answer(:,2) = (Answer(:,2) - yoff)*ymult + yzero;
            Answer(:,1) =  (0:xincr:xincr*(length(Answer)-1))';
            varargout{1} = Answer;
            varargout{2} = headerdata';

            
            if ~(length(Answer) == ByteNumber/2);
               warning('buggy') 
            end
            

            obj.Trace = Answer(:,1:2);
            % obj.VisaObject.BytesAvailable
            % hupe2 = fscanf(obj.VisaObject)
            % length(hupe2)
            % hupe3 = fscanf(obj.VisaObject)
            % hupe4 = fscanf(obj.VisaObject)
            % fgets(obj.VisaObject);
        end
        
    end