function OutputFormatIni(obj)
            % This function initializes that the output format is formatted
            % in the right way
            
            % Sets the output to be binary
            StrValue = 'RIB';
            PreCommand = ':DAT:ENC';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);

            % Sets the output to be a 2byte integer
            BytePerValue = 2;
            StrValue = num2str(BytePerValue);
            PreCommand = ':WFMO:BYT_N';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
        end