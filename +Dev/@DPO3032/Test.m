        function Answer = Test(obj)
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,'*WAI;:WFMPRE:YOFF?;:WFMPRE:YMULT?;:WFMPRE:YZERO?;:WFMPRE:XINCR?;:WFMPRE:XZERO?;:DAT:WID?'); % Communication with device
            Answer = DeviceAnswerRaw;
        end