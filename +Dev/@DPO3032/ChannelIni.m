        function ChannelIni(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            StrValue = obj.Channel;
            PreCommand = ':DATA:SOURCE';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
        end