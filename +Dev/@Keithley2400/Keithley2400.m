classdef Keithley2400 < Dev.SuperDev.SourceMeter
    %KEITHLEY2400 Summary of this class goes here
    %   Detailed explanation goes here
    properties
        VoltageCompliance;
        CurrentCompliance
        OutputState
        VoltageDC
        CurrentDC
    end
    
    
    methods
        function obj = Keithley2400(VisaAdress)
            if nargin ~= 0
            %KEITHLEY2400 Construct an instance of this class
            %   Detailed explanation goes here
            obj.VisaType = 'visa-gpib';
            obj.VisaAdress = VisaAdress;
            obj = obj.DevIni;
%             VisaWrite(obj, ':OUTP:STAT 0')
            VisaWrite(obj,'*RST');
            VisaRead(obj,'*OPC?');
            obj.VoltageCompliance = 2.1;
%             VisaWrite(obj, ':VOLT:PROT:LEV DEF');
            obj.CurrentCompliance = 1.05e-4;
%             VisaWrite(obj, ':CURR:PROT:LEV DEF');
            
            else
                obj.VisaType = 'visa-gpib';
            end
        end
        
        function Answer = get.VoltageCompliance(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            Command = ':VOLT:PROT:LEV?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
            if(abs(obj.VoltageCompliance - Answer) > 0.2)
                error('Device Voltage compliance deviates too far from saved compliance.');
            end
        end
        
        function Answer = get.CurrentCompliance(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            Command = ':CURR:PROT:LEV?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
            obj.CurrentCompliance = Answer;
            if(abs(obj.CurrentCompliance - Answer) >0.2)
                error('Device Current compliance deviates too far from saved compliance.');
            end
        end
        
        function Answer = get.OutputState(obj)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            Command = ':OUTP:STAT?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
            obj.OutputState = Answer;
        end
        
        function obj = set.VoltageCompliance(obj, val)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            Command = ':VOLT:PROT:LEV';
            Command = [Command ' ' num2str(val)];
            VisaWrite(obj,Command);
            Answer = VisaRead(obj,':VOLT:PROT:LEV?');
            Answer = str2double(Answer);
            obj.VoltageCompliance = Answer;
        end
        
        function obj = set.CurrentCompliance(obj, val)
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            Command = ':CURR:PROT:LEV';
            Command = [Command ' ' num2str(val)];
            VisaWrite(obj,Command);
            Answer = VisaRead(obj,':CURR:PROT:LEV?');
            Answer = str2double(Answer);
            obj.CurrentCompliance = Answer;
        end
        
        function write2DisplayTop(obj, charText)
            if ischar(charText) && length(charText) < 21
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            VisaWrite(obj,'DISP:ENAB 1');
            Command = ':DISP:WIND1:TEXT:DATA';
            Command = [Command ' ' charText];
            VisaWrite(obj,Command);
            else
                error('Display message must be char type and less than 20 characters long.')
            end
        end
        
        function write2DisplayBottom(obj, charText)
            if ischar(charText) && length(charText) < 33
            VisaWrite(obj,'DISP:ENAB 1');
            %METHOD1 Summary of this method goes here
            %   Detailed explanation goes here
            Command = ':DISP:WIND2:TEXT:DATA';
            Command = [Command ' ' charText];
            VisaWrite(obj,Command);
            else
                error('Display message must be char type and less than 32 characters long.')
            end
        end
        
        function volt = measureVoltwithCurr(obj, Amps)
            %Measure Voltage with Current Amps
            VisaWrite(obj,':SENS:FUNC:OFF:ALL');
            VisaWrite(obj,':SENS:FUNC ''VOLT''');
            VisaWrite(obj,':SENS:VOLT:RANG:AUTO 1');
            VisaWrite(obj,':SOUR:FUNC: CURR');
            Amps = num2str(Amps);
            VisaWrite(obj,[':SOUR:CURR:' ' ' Amps]);
            VisaWrite(obj,':SOUR:CLE:AUTO ON');
            VisaWrite(obj,'TRIG:COUN 1');
            VisaWrite(obj,'FORM:ELEM VOLT');
            volt = VisaRead(obj,':READ?');
            volt = str2double(volt);
        end
        
        function Res = measureResWithCurr(obj, Amps)
            %Measure Resistance with Current Amp
            VisaWrite(obj,':SENS:FUNC ''RES''');
            VisaWrite(obj,':SENS:RES:NPLC 1');
            VisaWrite(obj,':SOUR:RES:MODE MAN');
            VisaWrite(obj,[':SOUR:CURR:' ' ' Amps]);
            VisaWrite(obj,':SOUR:CLE:AUTO ON');
            VisaWrite(obj,':TRIG:COUN 1');
            VisaWrite(obj,':FORM:ELEM RES');
            Res = VisaRead(obj,':READ?');
            Res = str2double(Res);
        end
        
        function measureLinVoltageRamp(start, stop, step, delay)
                        %Measure Resistance with Current Amp
            VisaWrite(obj,[':SOUR:VOLT:START' ' ' start]);
            VisaWrite(obj,[':SOUR:VOLT:STOP' ' ' stop]);
            VisaWrite(obj,[':SOUR:VOLT:STEP' ' ' step]);
            VisaWrite(obj,[':SOUR:DEL' ' ' delay]);
            VisaWrite(obj,':SOUR:VOLT:MODE: SWE');
            VisaWrite(obj,':SOUR:SWE:RANG AUTO');
            TrigPoints = str2double(VisaRead(obj, ':SOUR:SWE:POIN?'));
            VisaWrite(obj,[':TRIG:COUN' ' ' TrigPoints]);
            VisaWrite(obj,':SOUR:CLE:AUTO ON');
            fprintf("Set Sweeping from %f V to %f V in %f V Steps with %f s Delay\n", [start ,stop , step, delay])
            VisaWrite(obj,':READ?');
        end
        
        function measureLinCurrentRamp(obj, start, stop, step, delay)
                        %Measure Resistance with Current Amp
            VisaWrite(obj,[':SOUR:CURR:START' ' ' start]);
            VisaWrite(obj,[':SOUR:CURR:STOP' ' ' stop]);
            VisaWrite(obj,[':SOUR:CURR:STEP' ' ' step]);
            VisaWrite(obj,[':SOUR:DEL' ' ' delay]);
            VisaWrite(obj,':SOUR:CURR:MODE: SWE');
            VisaWrite(obj,':SOUR:SWE:RANG AUTO');
            TrigPoints = str2double(VisaRead(obj, ':SOUR:SWE:POIN?'));
            VisaWrite(obj,[':TRIG:COUN' ' ' TrigPoints]);
            VisaWrite(obj,':SOUR:CLE:AUTO ON');
            fprintf("Set Sweeping from %f A to %f A in %f A Steps with %f s Delay\n", [start ,stop , step, delay])
            VisaWrite(obj,':READ?');
        end
        
        function setCurrent(obj, current)
        VisaWrite(obj,[':SOUR:CURR' ' ' current]);
        end
        
        function setVoltage(obj, volt)
        VisaWrite(obj,[':SOUR:VOLT' ' ' volt]);
        end
        
        function obj = set.OutputState(obj, boolean)
        VisaWrite(obj,[':OUTP:STAT' ' ' boolean]);
        obj.OutputState = boolean;
        end
    end
end


