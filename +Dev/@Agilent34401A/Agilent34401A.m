classdef Agilent34401A < Dev.SuperDev.Multimeter
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        VoltageDC
        VoltageAC
        Name
        CurrentDC
        CurrentAC
        Resistance
        Resistance4W
        Frequency
    end
    
    
    
    
    %     %% Private methods
    methods
        
        function obj = Agilent34401A(~)
            obj.VisaType = 'visa-gpib';
        end
        
        
        function Answer = get.VoltageDC(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:VOLT:DC?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
        
        
        function Answer = get.VoltageAC(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:VOLT:AC?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
        
        
        function Answer = get.CurrentDC(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:CURR:DC?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
        
        
        function Answer = get.CurrentAC(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:CURR:AC?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
        
        function Answer = get.Resistance(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:RES?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
        
        function Answer = get.Resistance4W(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:FRES?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
        
        function Answer = get.Frequency(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'MEAS:FREQ?';
            Answer = VisaRead(obj,Command);
            Answer = str2double(Answer);
        end
    end
    
end


