classdef  SR530LockIn < Dev.SuperDev.LockInAmp & Dev.Comm.VisaObject
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        
        Sensitivity
        TimeAverage
        Name
        OutputAmp
        OutputPhs
        Unlock
        Overload
        Frequency
        AutoInc = true;
        LaserObject = Dev.LaserObject;
        
        
    end
    
    properties (Access = private)
        
        G
        
        T
        
        SensList = ...
            [1 10e-9;...
            2 20e-9;...
            3 50e-9;...
            4 100e-9;...
            5 200e-9;...
            6 500e-9;...
            7 1e-6;...
            8 2e-6;...
            9 5e-6;...
            10 10e-6;...
            11 20e-6;...
            12 50e-6;...
            13 100e-6;...
            14 200e-6;...
            15 500e-6;...
            16 1e-3;...
            17 2e-3;...
            18 5e-3;...
            19 10e-3;...
            20 20e-3;...
            21 50e-3;...
            22 100e-3;...
            23 200e-3;...
            24 500e-3]
        
        
        TimeList = ...
            [1 1e-3;...
            2 3e-3;...
            3 10e-3;...
            4 30e-3;...
            5 100e-3;...
            6 300e-3;...
            7 1;...
            8 3;...
            9 10;...
            10 30;...
            11 100]
        
    end
    
    
    %% Communication METHODS
    methods (Access = private)
        
        
        function obj = SensInc(obj)
            
            Command = 'G';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            FirstSensitivity = str2double(DeviceAnswerRaw);
            obj.G = FirstSensitivity;
            
            SecondSensitivity  = min(FirstSensitivity + 1,24);
            
            if FirstSensitivity == 24;
                Dev.Util.cprintf([1,0,0],'Maximum Sensitivity has been reached');
            else
                
                
                Break = obj.Overload;
                Iterator = 0;
                NewSensValue = SecondSensitivity+Iterator;
                while and(Break == true,NewSensValue <= 24)
                    
                    
                    Command = ['G' ' '];
                    % Set the desired Current of the device
                    NewSensValue = SecondSensitivity+Iterator;
                    NewSensValue2 = obj.SensList(find(obj.SensList(:,1) == NewSensValue),2);
                    Dev.Util.cprintf([1,0,0],['New Sensitivity due to OVL:' ' ' num2str(NewSensValue2,'%1.2e') 'V\n']);
                    % Communication with device
                    obj.VisaWrite([Command num2str(NewSensValue)])
                    pause(obj.TimeAverage+0.1);
                    Iterator = Iterator + 1;
                    Break = obj.Overload;
                end
            end
            pause(0.2)
            
        end
        
        function obj = SensDec(obj)
            
            Command = 'Q1';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            OutputAmp = str2double(DeviceAnswerRaw);
            
            
            
            
            
            
            
            
            
            Command = 'G';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            FirstSensitivity = str2double(DeviceAnswerRaw);
            obj.G = FirstSensitivity;
            
            FirstSensValue2 = obj.SensList(find(obj.SensList(:,1) == FirstSensitivity),2);
            FirstSensValue3 = obj.SensList(find(obj.SensList(:,1) == FirstSensitivity-1),2);
            
            if ~and(OutputAmp<FirstSensValue2,OutputAmp>FirstSensValue3)
                
                
                SecondSensitivity  = max(FirstSensitivity - 1,1);
                
                if SecondSensitivity == 1;
                    Dev.Util.cprintf([1,0,0],'Minimum Sensitivity has been reached');
                else
                    Break = obj.Overload;
                    Iterator = 0;
                    NewSensValue = SecondSensitivity-Iterator;
                    while and(~Break,NewSensValue >= 1)
                        Command = ['G' ' '];
                        % Set the desired Current of the device
                        NewSensValue = SecondSensitivity-Iterator;
                        NewSensValue2 = obj.SensList(find(obj.SensList(:,1) == NewSensValue),2);
                        Dev.Util.cprintf([1,0,0],['New Sensitivity due to OVL:' ' ' num2str(NewSensValue2,'%1.2e') 'V\n']);
                        % Communication with device
                        obj.VisaWrite([Command num2str(NewSensValue)])
                        pause(obj.TimeAverage+0.1);
                        Iterator = Iterator + 1;
                        Break = obj.Overload;
                    end
                    obj.VisaWrite([Command num2str(NewSensValue+1)])
                    
                end
            end
            pause(0.2)
        end
        
        function obj = CheckUnlock(obj)
            Iterator = 0;
            while and(obj.Unlock,Iterator <=10)
                Iterator = Iterator + 1;
                pause(1);
            end
            
            if Iterator == 10
                Dev.Util.cprintf([1,0,0],['Measurement began before LockIn locked to Ref Frequency\n']);
                
            end
        end
        
        
    end
    
    
    %% GET METHODS
    
    methods
        
        function obj = SR530LockIn(~)
            obj.VisaType = 'visa-gpib';
            obj.T = 7;
            obj.G = 24;
        end
        
        function Answer = get.OutputAmp(obj)
            % Visa Read Voltage
            a=false;
            if obj.AutoInc 
                if obj.Overload
                    obj.SensInc;
                    a=true;
                elseif ~obj.Overload
                    obj.SensDec;
                    a=true;
                elseif obj.Unlock
                    obj.CheckUnlock;
                    a=false;
                end
            end
            if ~a
                pause(0.2)
            end
            
            Command = 'Q1';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            pause(0.01)
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswerRaw);
            
            
            ActSens = obj.Sensitivity;
            %             LowSens
            
        end
        
        
        
        function Answer = get.OutputPhs(obj)
            if obj.Overload
                obj.SensInc;
            elseif ~obj.Overload
                obj.SensDec;
            end
            % Visa Read actual current
            Command = 'Q2';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            pause(0.01)
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswerRaw);
        end
        
        
        
        function Answer = get.Sensitivity(obj)
            % Visa Read software limit of current
            
            Command = 'G';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            pause(0.01)
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswerRaw);
            obj.G = Answer;
            Answer = obj.SensList(find(obj.SensList(:,1) == Answer),2);
        end
        
        
        
        
        function Answer = get.TimeAverage(obj)
            % Visa Read if power output is on or off
            
            Command = 'T 1';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswerRaw);
            obj.T = Answer;
            Answer = obj.TimeList(find(obj.TimeList(:,1) == Answer),2);
        end
        
        
        function Answer = get.Frequency(obj)
            % Visa Read the chosen current
            
            Command = 'F';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            pause(0.01)
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswerRaw);
        end
        
        function Answer = get.Unlock(obj)
            % Visa Read the chosen current
            
            Command = 'Y 3';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            pause(0.01)
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            
            if strcmp(DeviceAnswerRaw(1),'0')
                Answer = false;
            elseif strcmp(DeviceAnswerRaw(1),'1')
                Answer = true;
            else
                error('Error in Reading out the Unlock Status Byte')
            end
            
        end
        
        function Answer = get.Overload(obj)
            % Visa Read the chosen current
            
            Command = 'Y 4';
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            pause(0.01)
            DeviceAnswerRaw = VisaRead(obj,Command); % Communication with device
            
            if strcmp(DeviceAnswerRaw(1),'0')
                Answer = false;
            elseif strcmp(DeviceAnswerRaw(1),'1')
                Answer = true;
            else
                error('Error in Reading out the Overload Status Byte')
            end
        end
        
        
    end
    
    
    
    
    
    
    
    %% SET methods
    
    
    
    methods
        
        
        function obj = set.Sensitivity(obj,Value)
            
            Index = find(obj.SensList(:,2) == Value);
            obj.G = obj.SensList(Index,1);
            
            
            Command = ['G' ' '];
            % Set the desired Current of the device
            
            % Communication with device
            obj.VisaWrite([Command num2str(obj.G)])
            
            
        end
        
        
        
        
        function obj = set.TimeAverage(obj,Value)
            % Enables or disables the output
            
            Index = find(obj.TimeList(:,2) == Value);
            obj.T = obj.TimeList(Index,1);
            
            
            Command = ['T 1,'];
            % Set the desired Current of the device
            
            % Communication with device
            obj.VisaWrite([Command num2str(obj.T)])
            
        end
        
        
        
        
    end
end



