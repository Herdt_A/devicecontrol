classdef ICL2183_10_12 < Dev.LaserObject
    
    
    properties
    end
    
    methods
        function obj = ICL2183_10_12(~)
            obj.Current_Lim = 0.065; % REAL 0.080
            obj.Temp_Min = 10;  % REAL 0
            obj.Temp_Max = 35; % REAL 40
            obj.Voltage_Lim = 5;  % Has no impact
            obj.Wavelength = 3244.8;
            
        end
        
        
        
    end
end


