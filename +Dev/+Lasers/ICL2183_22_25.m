classdef ICL2183_22_25 < Dev.LaserObject
    
    
    properties
    end
    
    methods
        function obj = ICL2183_13_25(~)
            obj.Current_Lim = 0.065; % REAL 0.080
            obj.Temp_Min = 15;  % REAL 5
            obj.Temp_Max = 30; % REAL 35
            obj.Voltage_Lim = 5;  % Has no impact
            obj.Wavelength = 3240.67;
            
        end
        
        
        
    end
end




%
% ICL2183_13_25.Current_Lim = 0.065; % REAL 0.080
% ICL2183_13_25.Temp_Min = 15;  % REAL 5
% ICL2183_13_25.Temp_Max = 30; % REAL 35
% ICL2183_13_25.Voltage_Lim = 5;  % Has no impact
% ICL2183_13_25.Wavelength = 3240.6;
