function [Output] = SafeToAsci_extern(Data,FileFolder,FileName,varargin)
%SAFEESATRACE Summary of this function goes here
%   Detailed explanation goes here
% 
% UserName = getenv('username');
% RootFolder = ['C:\Users\' UserName '\Nextcloud\Measurement\' datestr(datetime('now'),'yyyymmdd') ];


if ~isempty(varargin)
    Del = varargin{1};
else
    Del = ';';
end

if exist(FileFolder,'dir') ~= 7
mkdir(FileFolder)
end

filename = strcat(FileFolder,'\',FileName,'.csv');
dlmwrite(filename,Data, 'delimiter', Del,'precision','%1.6e');
% % fclose(Osci1.VisaObject);
% delete(Osci1.VisaObject);
Output = true;

end

