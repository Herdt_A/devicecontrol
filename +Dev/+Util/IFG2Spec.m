% clc
% clear variables
% close all
% 
% 
function [FFT_Out] = IFG2Spec(Filename, LowCutoff, HighCutoff)

ReadIn = Dev.Util.OpusReadIn(Filename);
Data = ReadIn(:,2);
len_data = length(Data);
Wavelength = 632.8e-9;

Nyquist = 1e-2/(Wavelength/2);
deltaNu = Nyquist / len_data;

FFT = abs(real(fft(Data)));
inu = 1:floor(length(FFT)/2);
dnu = (inu-1)*deltaNu;
wavelengths = 1./(1e2*dnu);
FFT3 = FFT(inu);
% plot(wavelengths,FFT3)
% xlim([3000 3400]*1e-9)

outliers = ~excludedata(wavelengths',FFT3,'domain',[LowCutoff HighCutoff]*1e-9);

wavelengths2 = wavelengths(outliers)/1e-9;
wavelengths2 = wavelengths2';
FFT2 = FFT3(outliers);
FFT_Out = [wavelengths2 FFT2];

% figure
% plot(wavelengths2,FFT2);
% YLim = get(gca,'YLim');
% YLim = YLim(2)/20;
% 
% 
% l1 = legend(FileNames{i}(3:8));
% 
% [pks{i},locs] = findpeaks(FFT2,'MinPeakProminence',YLim,'MinPeakDistance',100,'MinPeakHeight',0.02);
% 
% peakwavelengths{i} = wavelengths2(locs);
% 
% hold on
% plot(peakwavelengths{i},pks{i},'x')
% drawnow();
% hold off
end