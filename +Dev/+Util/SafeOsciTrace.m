function [Output] = SafeOsciTrace(Data,FileFolder,FileName,varargin)
%SAFEESATRACE Summary of this function goes here
%   Detailed explanation goes here

UserName = getenv('username');
RootFolder = ['C:\Users\' UserName '\Nextcloud\Measurement\' datestr(datetime('now'),'yyyymmdd') ];


if ~isempty(varargin)
    RootFolder = varargin{1};
    
end

if exist([RootFolder,'\',FileFolder],'dir') ~= 7
    mkdir([RootFolder,'\',FileFolder])
end

filename = strcat(RootFolder,'\',FileFolder,'\',FileName,'.oscitrc');



Dev.Util.cprintf('*[1,0.5,0]',['Saved as:' ' ' strrep(filename,'\','/') '\n'])
% dlmwrite(filename,Data, 'delimiter', ';')
fileID = fopen(filename,'w');
fwrite(fileID,[Data(1,1) Data(end,1) size(Data,1) size(Data,2)-1],'float');
fclose(fileID);



fileID = fopen(filename,'a');
fwrite(fileID,Data(:,2:end),'float');
fclose(fileID);
Output = true;

end

