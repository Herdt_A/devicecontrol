function Data = OpusReadIn(Filename)
%OPUSREADIN Summary of this function goes here
%   Detailed explanation goes here
    Wavelength = 632.8e-9;
    
    FID = fopen(Filename);
    DataRead1 = fread(FID, 'float');
    Data(:,2) = DataRead1(37:end-352);
    Data(:,1) = linspace(0,length(Data(:,2))*Wavelength/2,length(Data(:,2)));
end

