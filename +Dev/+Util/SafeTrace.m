function [Output] = SafeTrace(obj,FileFolder,FileName,varargin)
%SAFEESATRACE Summary of this function goes here
%   Detailed explanation goes here

UserName = getenv('username');
RootFolder = ['C:\Users\' UserName '\Nextcloud\Measurement\' datestr(datetime('now'),'yyyymmdd') ];

if ~isempty(varargin)
    RootFolder = [RootFolder '_' varargin{1}];
end
mkdir([RootFolder,'\',FileFolder])
filename = strcat(RootFolder,'\',FileFolder,'\',FileName,'.trc');
dlmwrite(filename,obj.Trace)
% % fclose(Osci1.VisaObject);
% delete(Osci1.VisaObject);

Output = true;
end

