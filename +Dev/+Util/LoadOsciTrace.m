function [Data_out,Output] = LoadOsciTrace(FileFolder,FileName,varargin)
%SAFEESATRACE Summary of this function goes here
%   Detailed explanation goes here

% UserName = getenv('username');
% RootFolder = ['C:\Users\' UserName '\Nextcloud\Measurement\' datestr(datetime('now'),'yyyymmdd') ];


% if ~isempty(varargin)
%     RootFolder = [RootFolder '_' varargin{1}];
%     
% end
% mkdir([RootFolder,'\',FileFolder])

filename = strcat(FileFolder,'\',FileName);
% dlmwrite(filename,Data, 'delimiter', ';')
fileID = fopen(filename,'r');
Data = fread(fileID,'float');
DataLength = Data(3);
ChannelsAvailable = Data(4);
Data_out(:,1) = linspace(Data(1),Data(2),Data(3));
Data_out2 = reshape(Data(5:end),[DataLength ChannelsAvailable]);
Data_out = [Data_out Data_out2];
fclose(fileID);
% % fclose(Osci1.VisaObject);
% delete(Osci1.VisaObject);
Output = true;

end

