classdef LaserObject
    %LaserObject is a superclass for all devices which are associated with a specific laser
    % Name            (Name of the Laser (string)) default = Laser1
    % Type            (Lasertype DFB, FP or DM)
    % Status          (on or off (true or false))
    % Current_Lim     (Current Limit of Laser (double > 0)) in A
    % Voltage_Lim     (Voltage Limit of Laser (double > 0)) in V
    % Current         (Actual Current (double > 0)) in A
    % Voltage         (Actual Voltage (double > 0)) in V
    % Wavelength      (Actual Wavelength (double > 0)) in nm
    % LastReadOut     (Last time the actual values have been updated)
    
    properties
        % Misc
        Name              = 'Laser1'
        Type
        Status
        TEC_Status
        
        % Limits
        Current_Lim
        Voltage_Lim
        Temp_Min = 22.5;
        Temp_Max = 23.6;
        
        
        % Acutal values
        Current
        Voltage
        Temp
        Wavelength
        LastUpdate
        
    end
    
    
    %% SET METHODS
    methods
        function obj = set.LastUpdate(obj,Value)
            % Set the last read out of the laser values
            
            % Check input
            if ~isa(Value,'datetime') % input must be a float
                error('Current Input must be of class datetime')
            end
            obj.LastUpdate = Value;
        end
        
        
        
        function obj = set.Temp_Min(obj,Value)
            % Set the desired Current of the laser
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Current Input must be a number')
            elseif Value < 0
                error('Current Input must be a positive number')
            end
            
            if ~isempty(obj.Temp_Max)
                if Value >= obj.Temp_Max
                    error('Value must be smaller than maximal temperature')
                end
            end
            obj.Temp_Min = Value;
        end
        
        function obj = set.Temp_Max(obj,Value)
            % Set the desired Current of the laser
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Current Input must be a number')
            elseif Value < 0
                error('Current Input must be a positive number')
            end
            
            if ~isempty(obj.Temp_Min)
                if Value <= obj.Temp_Min
                    error('Value must be greater than minimal temperature')
                end
            end
            obj.Temp_Max = Value;
        end
        
        
        function obj = set.Current(obj,Value)
            % Set the desired Current of the laser
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Current Input must be a number')
            elseif Value < 0
                error('Current Input must be a positive number')
            end

            obj.Current = Value;
                        obj.LastUpdate = datetime('now');
        end
        
        function obj = set.Voltage(obj,Value)
            % Set the desired Voltage of the laser
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Voltage Input must be a number')
            elseif Value < 0
                error('Voltage Input must be a positive number')
            end

            obj.Voltage = Value;
                        obj.LastUpdate = datetime('now');
        end
        
        function obj = set.Wavelength(obj,Value)
            % Set the desired Wavelength of the laser
            
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Wavelength Input must be a number')
            elseif Value < 0
                error('Wavelength Input must be a positive number')
            end

            obj.Wavelength = Value;
                        obj.LastUpdate = datetime('now');
        end
        
        function obj = set.Current_Lim(obj,Value)
            % Set the desired Current Limit of the laser
            
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Current_Lim Input must be a number')
            elseif Value < 0
                error('Current_Lim Input must be a positive number')
            end
            obj.Current_Lim = Value;
        end
        
        function obj = set.Voltage_Lim(obj,Value)
            % Set the desired Voltage Limit of the laser
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Voltage_Lim Input must be a number')
            elseif Value < 0
                error('Voltage_Lim Input must be a positive number')
            end
            obj.Voltage_Lim = Value;
        end
        
        
        function obj = set.Name(obj,Value)
            % Set the desired name of the laser
            
            
            % Check input
            if ~ischar(Value) % input must be a float
                error('Name Input must be a string')
            end
            obj.Name = Value;
        end
        
        function obj = set.Type(obj,Value)
            % Set the desired type of the laser
            
            
            % Check input
            if ~ischar(Value) % input must be a char
                error('Type Input must be a string')
            end
            obj.Current = Value;
        end
        
        function obj = set.Status(obj,Value)
            % Set the desired type of the laser
            % Check input
            if ~or(Value==false,Value==true) % input must be a boolean
                error('Status Input must be either 0 (off) or 1 (on)')
            end
            obj.Status = Value;
            obj.LastUpdate = datetime('now');
        end
        
        function obj = set.TEC_Status(obj,Value)
            % Set the desired type of the laser
            % Check input
            if ~or(Value==false,Value==true) % input must be a boolean
                error('Status Input must be either 0 (off) or 1 (on)')
            end
            obj.TEC_Status = Value;
            obj.LastUpdate = datetime('now');
        end
        
        
        
    end
    
end

