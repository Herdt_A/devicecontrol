classdef ThorProLDC < Dev.SuperDev.ThorPro800 & Dev.SuperDev.LDC
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        % THOR PRO SPECIFIC PROPERTIES
        HLimCurrent   % hardware limit of current
        LDPol        % Pol
        
        
        % INHERITED PROPERTIES
        CurrentUnit = 1;
        Name = 'ThorLDC 1';
        ActCurrent    % actual current of the Laser
        SetCurrent    % set current
        SLimCurrent   % software limit of current
        Output       % Output on / Output off
        Voltage      % Voltage of the device
        ConnectedTEC
        
        LaserObject = Dev.LaserObject;
        
  

end
    

        

        
        
      
    
    %% GET methods
    methods
        
%         function getCurrentUnit(obj)
%             getCurrentUnit@Dev.SuperDev.LDC(obj.CurrentUnit);
%         end

   
       
        function Answer = get.Voltage(obj)
            % Visa Read Voltage          
                        obj.SlotIni; % needed to choose correct slot
            Command = ':VLD:ACT?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        
        function Answer = get.ActCurrent(obj)
            % Visa Read actual current     
                        obj.SlotIni; % needed to choose correct slot
            Command = ':ILD:ACT?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
            Answer = Answer*obj.CurrentUnit;
        end
        
        function Answer = get.SLimCurrent(obj)
            % Visa Read software limit of current
            obj.SlotIni; % needed to choose correct slot
            Command = ':LIMC:SET?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
            Answer = Answer*obj.CurrentUnit;
        end
        
        
        
        
        function Answer = get.Output(obj)
            % Visa Read if power output is on or off
                        obj.SlotIni; % needed to choose correct slot
            Command = ':LASER?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            
            
            if strcmp(DeviceAnswer(1:3),'OFF')
                Answer = false;
            elseif strcmp(DeviceAnswer(1:2),'ON')
                Answer = true;
            else
                error('Output Read Error')
            end
            
            
        end
        
        
        
        
        function Answer = get.SetCurrent(obj)
            % Visa Read the chosen current
            
                        obj.SlotIni; % needed to choose correct slot

            
            
            
            Command = ':ILD:SET?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
            Answer = Answer*obj.CurrentUnit;
        end







        function Answer = get.HLimCurrent(obj)
            getCurrentUnit(obj);
            % Visa Read hardware limit of current
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':LIMCP:ACT?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        
        function Answer = get.LDPol(obj)
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':LDPOL?'); % Communication with device
            
            % Convert Answer to String
            if strcmp(DeviceAnswer(1:2),'CG')
                Answer = 'CG';
            elseif strcmp(DeviceAnswer(1:2),'AG')
                Answer = 'AG';
            else
                error('Output Readout ERROR')
            end
            
        end
    end
    
    
    
    
    
    
    
    %% SET methods
    methods
        
        
           
        function DeviceWrite(obj,PreCommand,Value)
             % Convert input to float number with exponential and that to string
%             PreCommand = obj.CommandsList{find(strcmp(obj.VariableList,'sSetCurrent')==1)};
            Command = [PreCommand ' ' Value];
            VisaWrite(obj,Command);
        end
        
        
    end
    
    
    methods
        
        
        function obj = set.SetCurrent(obj,Value)
            obj.SlotIni(); 
            CheckSetCurrent(obj,Value)
            
            Value = Value/obj.CurrentUnit;
            Command = ':ILD:SET';
            % Set the desired Current of the device
            StrValue = num2str(Value,'%e');
            % Communication with device
            obj.DeviceWrite(Command,StrValue)
            

        end
        
        
        
        
        function obj = set.Output(obj,Value)
            % Enables or disables the output
           Command = ':LASER';
           obj.SlotIni(); 
           Switching = obj.CheckSwitchOutput(Value);
            
           if Switching == 1
               Value = 'ON';
           elseif Switching == 0
               Value = 'OFF';
           elseif Switching == -1
               Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': Device Output is already on'])
           end
           obj.DeviceWrite(Command,Value)

            % The controller
        obj.CheckInterLock(Value);
        end
        
        
        
        
        
        
        
        function obj = set.SLimCurrent(obj,Value)
            obj.SlotIni(); 
            Value = Value/obj.CurrentUnit;
            Command = ':LIMC:SET';
            
            LimValue = obj.CheckSLimCurrent(Value);

            
            if LimValue == true
                            StrValue = num2str(Value,'%e');

                obj.DeviceWrite(Command,StrValue);
            end
        end
        
        
        
        
        % Due to security reasons some Values are not allowed to be written to the
        % device (e.g. software limit). Some values also can't be written, because the device
        % produces them itself (e.g. Voltage).
        function obj = set.ActCurrent(obj,Value)
            error('Can only be read. To change current, change SetCurrent!')
        end
        
        
        
        function obj = set.Voltage(obj,Value)
            error('Can only be read. Device property.')
        end
  

        % Due to security reasons some Values are not allowed to be written to the
        % device (e.g. software limit). Some values also can't be written, because the device
        % produces them itself (e.g. Voltage).
        
        function obj = set.HLimCurrent(obj,Value)
            error('Can only be read due to safety reasons - Change at device!')
        end

        
        function obj = set.LDPol(obj,Value)
            error('Can only be read due to safety reasons - Change at device')
        end
        
        
        
        
        
        
    end
    
end


