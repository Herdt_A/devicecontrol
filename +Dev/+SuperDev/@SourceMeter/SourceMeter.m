classdef SourceMeter < Dev.Comm.VisaObject
    %SOURCEMETER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (Abstract)
        VoltageCompliance
        CurrentCompliance
        OutputState
        VoltageDC
        CurrentDC
    end
    
end

