function [varargout] = goSafe2Current(obj,Value,varargin)
% goSafe2Current(obj,EndCurrent,float(Speed),boolean(ShowSteps))
% Increases or decreases the Current to EndCurrent.
% Speed is a number and is given in mA per second.
% Standard Speed is 2mA per second
% ShowSteps is true or false and visualizes the steps.


% Safety checks

% Only starts GoSafe2Current if output is turned on
if obj.Output == false
    error('Output turned off. To go safe to current, turn output on!')
end

% Only starts GoSafe2Current if Hardwarelimit and Softwarelimit
% are not exceeded.
if isempty(obj.LaserObject.Current_Lim)
    error('LaserObject need Current Limit!')
end


if obj.SLimCurrent > obj.LaserObject.Current_Lim
    error('Software Limit is bigger than Current Limit of Laser!')
end

if Value > obj.SLimCurrent
    error(['Final goSafe2Current Value violates Software Limit!!! Remember: Current Input is in A! Desired:' num2str(Value) 'A Limit:' ' ' num2str(obj.SLimCurrent) 'A'])
elseif Value >= obj.LaserObject.Current_Lim
    error(['Set goSafe2Current Value violates Laser Limit!!! Remember: Current Input is in A! Desired:' num2str(Value) 'A Limit:' ' ' num2str(obj.LaserObject.Current_Lim) 'A ########## !!!! Reset Software Limit !!!!'])
    
end

% Precalculations
Difference = Value - obj.ActCurrent; % Difference between recent current and EndCurrent
StartCurrent = obj.ActCurrent; % Safe recent current in variable


% Initialze Values
StepSize = 0.001; % 1mA Steps
Show = false; % Visualizer of acutal output

% Create Speed Value if no input is given
if isempty(varargin)
    Speed = 2; % Definition of Standard Speed
else
    Speed = varargin{1}; % Update Speed if required
    if length(varargin) >= 2
        Show = varargin{2}; % Update Visualizer if required
    end
end



% Communication speed is limited by ThorPro. To increase speed
% the stepsize has to be updated to higher values. Maximum
% current per second is
if Speed <= 4
elseif and(Speed > 4,Speed<=8);
    disp('goSafe2Current increased Stepsize to 2mA')
    StepSize = 0.002; % 2mA steps
    Speed = Speed/2; % Devide speed again
elseif and(Speed > 8,Speed<=16);
    disp('goSafe2Current increased Stepsize to 4mA')
    StepSize = 0.004; % 2mA steps
    Speed = Speed/4;
elseif and(Speed > 16,Speed<=32);
    disp('goSafe2Current increased Stepsize to 8mA')
    StepSize = 0.008; % 2mA steps
    Speed = Speed/8;
else
    error('goSafe2Current cant go faster than 32mA per second! StepSize max = 8mA due to safety reasons')
end


StepNumber = floor(Difference / StepSize); % Calculate Stepnumber



% Create Waitbar handle or create "warning message" in the case
% the set current = actual current
if ~(StepNumber == 0)
    %                 disp([obj.Name ': ' 'goSafe2Current from:' ' ' num2str(round(obj.ActCurrent*1000),2) 'mA to' ' ' num2str(Value*1000) 'mA'])
    f = waitbar(0,[obj.Name ': ' 'Go to current ' num2str(Value*1000) 'mA: ' num2str(round(obj.ActCurrent*1000),2) 'mA']);
else
    disp('goSafe2Current failed: Set Current = Act Current')
end


% Reverse Stepsize is SetCurrent < Actcurrent to obtain
% positive stepnumber
if StepNumber < 0;
    StepSize = -StepSize;
    StepNumber = -StepNumber;
end


% Initialize Output Current and Output Voltage, which are
% arrays that track Voltage and Current of the device during
% the goSafe2Current process
OutCurrent(1:floor(Value)) = nan;
OutVoltage(1:floor(Value)) = nan;
LoopTime(1:floor(Value)) = nan;
TimeStart = tic;

% Actual current iteration
for i=1:floor(StepNumber)
    waitbar(i/(floor(StepNumber)+1),f,[obj.Name ': ' 'Go from' ' ' num2str(round(StartCurrent*1000,1)) 'mA to' ' ' num2str(Value*1000) 'mA: ' num2str(round(obj.ActCurrent*1000),2) 'mA']);    % update Waitbar
    
    % Create variable that measures the time it takes for a
    % readout
    ReadOutTimeStart = tic;
    
    
    % Iterator
    obj.SetCurrent = StartCurrent + i*StepSize;
    
    % Display the actual current in the console, if varargin
    if Show
        disp([num2str(obj.ActCurrent*1000) ' ' 'mA'])
    end
    
    
    OutCurrent(i) = obj.ActCurrent; % Update Current Array
    OutVoltage(i) = obj.Voltage; % Update Voltage Array
    LoopTime(i) = toc(TimeStart); % Update Time Array
    
    ReadOutTime = toc(ReadOutTimeStart); % Get time it need to read out the device
    
    pause(1/Speed-ReadOutTime) % pause the right time. 1/Speed is the time in seconds it should take for a loop in order to achieve the right speed, but you have to substract the ReadOutTime of the device
    
end


% After the Loop is finished set the Current value the input
% Value and substract LoopTime, so that Looptime(1) = 0.
obj.SetCurrent = Value;
if ~(StepNumber ==0)
LoopTime = LoopTime - LoopTime(1);
end

% This is for deleting the Waitbar
if ~(StepNumber ==0) % Needed to check because delete(f) doesnt work if not
    waitbar(1,f,['Go to current ' num2str(Value*1000) 'mA: ' num2str(round(obj.ActCurrent*1000),2) 'mA']);  % 100%
    pause(.5) % Wait so that you can see 100%
    delete(f) % Close the waitbar
end


% If varargin Show  == true then show the actual current.
if Show
    disp([num2str(obj.ActCurrent*1000) ' ' 'mA'])
end

% Concatenate Time, Current and Voltage in one Array
varargout{1} = [LoopTime' OutCurrent' OutVoltage'];


end