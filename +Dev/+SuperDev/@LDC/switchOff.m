  function switchOff(obj)
            % switchOff(ThorLDCObject)
            % Switches ThorLDC safely off by turning the current slowly
            % down.
            
            % Check if output = on or off. If output is already off do
            % nothing.
            if obj.Output == true
                obj.goSafe2Current(0); % go slowly to 0mA
                % Wait 2 Seconds to Switch off
                disp([obj.Name ' ' 'switches off in 2s'])
                pause(2)
                % Switch off and display success
                obj.Output = false;
                disp([obj.Name ' ' 'successfully switched off'])
            else
                disp([obj.Name ' ' 'already switched off'])
            end
        end