  function switchOn(obj,Value)
            % switchOff(ThorLDCObject)
            % Switches ThorLDC safely off by turning the current slowly
            % down.
            
            % Check if output = on or off. If output is already off do
            % nothing.
            if obj.Output == false

                % Wait 2 Seconds to Switch off
                Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ' ' 'switches on in 2s\n'])
                pause(2)
                obj.Output = true;
                obj.goSafe2Current(Value); % go slowly to 0mA

                % Switch off and display success
%                 obj.Output = false;
                Dev.Util.cprintf([0,.5,0],[obj.Name ' ' 'successfully switched on to' ' ' num2str(round(Value*1000,1)) 'mA\n'])
            else
                disp([obj.Name ' ' 'already switched on'])
            end
        end