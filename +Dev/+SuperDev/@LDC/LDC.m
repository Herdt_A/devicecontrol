classdef LDC
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    

    
    properties (Abstract)
        CurrentUnit
        Name
                %General TEC PROPERTIES
        ActCurrent    % actual current of the Laser
        SetCurrent    % set current
        SLimCurrent   % software limit of current
        Output       % Output on / Output off
        Voltage      % Voltage of the device
        ConnectedTEC  

        LaserObject 

    end
    
    
    %% Other THOR LDC specific methods
    % See Folder
    % switchOn(Value)
    % switchOff
    % goSafe2Current(Value)
    
    
    
    
    
    
    %% Check Functions
    methods (Access = protected)
        
        function CheckSetCurrent(obj,Value)
                    % Check input
            if ~isfloat(Value) % input must be a float
                error('Current Input must be a number')
            end
            if isempty(obj.LaserObject.Current_Lim)
                error('LaserObject need Current Limit!')
            end
            
            if obj.SLimCurrent > obj.LaserObject.Current_Lim
                error('Software Limit is bigger than Current Limit of Laser!')
            end
            % Check so that Hardware and Software limit are not exceeded
            if Value >= obj.SLimCurrent
                error(['Set Value violates Software Limit!!! Remember: Current Input is in A! Desired:' num2str(Value) 'A Limit:' ' ' num2str(obj.SLimCurrent) 'A'])
            elseif Value >= obj.LaserObject.Current_Lim
                error(['Set Value violates Laser Limit!!! Remember: Current Input is in A! Desired:' num2str(Value) 'A Limit:' ' ' num2str(obj.LaserObject.Current_Lim) 'A ########## !!!! Reset Software Limit !!!!'])
            end
        end
        
        
        function CheckInterLock(obj,Value)
            if and(obj.Output == false,Value== true)
                error('Check Interlock')
            end
        end
        
        
        function StrValue = CheckSwitchOutput(obj,Value)
            
            if isempty(obj.LaserObject.Current_Lim)
                error('LaserObject need Current Limit!')
            end
            
            if obj.SLimCurrent > obj.LaserObject.Current_Lim
                error('Software Limit is bigger than Current Limit of Laser!')
            end
            
            if obj.SetCurrent >= obj.SLimCurrent
                error(['Set Value violates Software Limit!!! Remember: Current Input is in A! Desired:' num2str(obj.SetCurrent) 'A Limit:' ' ' num2str(obj.SLimCurrent) 'A'])
            elseif obj.SetCurrent >= obj.LaserObject.Current_Lim
                error(['Set Value violates Laser Limit!!! Remember: Current Input is in A! Desired:' num2str(obj.SetCurrent) 'A Limit:' ' ' num2str(obj.LaserObject.Current_Lim) 'A ########## !!!! Reset Software Limit !!!!'])
            end
            
            % Check input and convert Output-boolean to String depending on
            % the recent state of the output.
            
            
            % ALREADY TURNED ON
            if and(Value == true,obj.Output == true) % Desire: Turn on. Check if already turned on
                Dev.Util.cprintf([0,.5,0],[obj.Name ' ' 'already turned on\n']) % create warning if already turned on
                StrValue = -1;
            
                % ALREADY TURNED OFF
            elseif and(Value == false,obj.Output == false) % Desire: Turn off. Check if already turned off, but send the command anyway.
                Dev.Util.cprintf([0,.5,0],[obj.Name ' ' 'already turned off - however communicating\n'])  % Create Warning
                StrValue = 0;
            
                % TURN ON
            elseif and(Value == true,obj.Output == false); % Desire: Turn on. Check if turned off.
                
                % Set String to send to "on"
                StrValue = 1;
                
                % If there is a connceted TEC, check if the output of it is
                % enabeled, otherwise create warning
                if ~isempty(obj.ConnectedTEC)
                    if obj.ConnectedTEC.Output == false
                        error('TEC must be turned on to turn on laser')
                    end
                end
                
                
                % Set SetCurrent to 0 in order to prevent that the current
                % is very high when the laser is turned on
                if and(Value,~(obj.SetCurrent == 0));
                    Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': For turning on the Current is set to 0mA\n'])
                    obj.SetCurrent = 0;
                end
                
                % TURN OFF
            elseif and(Value == false,obj.Output == true) % Desire: Turn off. Check if turned on.
                % Set String to send to "off"
                StrValue = 0;
                
            else
                error('CheckSwitchOutput ERROR #1')
            end
            
            % Communication with device
            if StrValue == 1 % if device is already turned on - cancel communication to save time
                

                    Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': Switching on requires waiting of 2 seconds - Safety reason.\n'])
                    pause(2)

                

            end
            
        end
        
        
        
        
        
        
        function LimValue = CheckSLimCurrent(obj,Value)
                    LimValue= false;
            if ~isempty(obj.LaserObject.Current_Lim)
                if Value <= obj.LaserObject.Current_Lim
                    if Value <= round(obj.SLimCurrent,4);
                        LimValue= true;
                    else
                        Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': Software Limit can only be equal or LOWER than previous value.\n Desired:' ' ' num2str(Value*1000) 'mA\n Previous:' ' ' num2str(obj.SLimCurrent*1000) 'mA\n'])
                    end
                else
                    Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': Software Limit can only be  LOWER than Value of Laser Object.\n Desired:' ' ' num2str(Value*1000) 'mA\n LaserObject:' ' ' num2str(obj.LaserObject.Current_Lim*1000) 'mA\n'])                    
                end
            else
                error('LaserObject need Current Limit!')
                
                
            end
        end
        

        
        
    end
    
end


