function [varargout] = goSafe2Temp(obj,Value,varargin)
            if obj.Output == false
                error('Output turned off. To go safe to current, turn output on')
            end
            
            if Value > obj.LaserObject.Temp_Max
                error([obj.Name ':' 'goSafe2Temp - Final temperature value violates maximum temperature!'])
            elseif Value < obj.LaserObject.Temp_Min
                error([obj.Name ':' 'goSafe2Temp - Final temperature value violates minimum temperature!'])
            end
            
            Difference = Value - obj.ActTemp;
            StartTemp = obj.ActTemp;
            %             EndCurrent = Value;
            StepSize = 1/60;
            
            
            Show = false;
            if isempty(varargin)
                Speed = 1*60;
            else
                Speed = varargin{1}*60;
                if length(varargin)>2
                    Show = varargin{2};
                end
            end
            
            KperMin= round(Speed/60,1);
            
            
            if Speed <= 1*60
            elseif and(Speed > 1*60,Speed<=2*60);
                disp('goSafe2Temp increased Stepsize to .2K')
                StepSize = 2/60;
                Speed = Speed/2;
            elseif and(Speed > 2*60,Speed<=4*60);
                disp('goSafe2Temp increased Stepsize to .4K')
                StepSize = 4/60;
                Speed = Speed/4;
            elseif and(Speed > 4*60,Speed<=8*60);
                disp('goSafe2Temp increased Stepsize to .8K')
                StepSize = 8/60;
                Speed = Speed/8;
            else
                error('goSafe2Temp cant go faster than 8K per Minute due to safety reasons')
            end
            
            
            StepNumber = Difference / StepSize;
            StepNumber = floor(StepNumber);
            
            
            
            if ~(StepNumber == 0)
                disp([obj.Name ': ' 'goSafe2Temp from:' ' ' num2str(round(obj.ActTemp,2)) 'C to' ' ' num2str(round(Value,2)) 'C'])
                f = waitbar(0,[obj.Name ': ' 'Go to Temp ' num2str(round(Value,2)) 'C: ' num2str(round(obj.ActTemp,2)) 'C']);
            end
            
            if StepNumber < 0;
                StepSize = -StepSize;
                StepNumber = -StepNumber;
            elseif StepNumber == 0
                
                disp('goSafe2Current failed: Set Current = Act Current')
            end
            
            OutCurrent = [];
            OutVoltage = [];
            OutSetTemp = [];
            OutActTemp = [];
            TimeArray = [];
            
            
            
            TimeStart = tic;
            for i=1:floor(StepNumber)
                tic
                waitbar(i/(floor(StepNumber)+1),f,[obj.Name ': ' 'Go from' ' ' num2str(round(StartTemp,2)) '�C' ' ' 'to ' num2str(round(Value,2)) '�C: ' num2str(round(obj.ActTemp,2)) 'C with' ' ' num2str(KperMin) 'K per min' ]);
                
                TimeStart2 = tic;
                obj.SetTemp = StartTemp + i*StepSize;
                if Show
                    disp([num2str(obj.ActTemp) ' ' 'mA'])
                end
                
                OutCurrent(i) = obj.PeltCurrent;
                OutVoltage(i) = obj.Voltage;
                OutSetTemp(i) = obj.SetTemp;
                OutActTemp(i) = obj.ActTemp;
                TimeArray(i) = toc(TimeStart);
                TimeArray(i);
                %                 plot(TimeArray,OutActTemp)
                WaitTime = toc(TimeStart2);
                %                 1/(Speed/StepNumber)*abs(StepSize)*60
                pause(1/(Speed/StepNumber)*abs(StepSize)*60-WaitTime)
            end
            
            obj.SetTemp = Value;
            if ~(StepNumber ==0)
                waitbar(1,f,[obj.Name ': ' 'Go to Temp ' num2str(round(Value,2)) 'C: ' num2str(round(obj.ActTemp,2)) 'C']);
                TimeArray = TimeArray - TimeArray(1);
                pause(.5)
                delete(f)
            end
            if Show
                disp([num2str(obj.ActTemp) 'C'])
            end
            varargout{1} = [TimeArray' OutSetTemp' OutActTemp' OutCurrent' OutVoltage'];
            
            
            
        end