classdef TEC
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        
        %General TEC PROPERTIES
        ActTemp % actual Temperature current of the TEC
        PeltCurrent    % actual Peltier current of the TEC
        SLimCurrent   % Software limit of Peltier current
        Voltage      % Voltage of the device
        SetTemp % set Temperature current of the TEC
        Output       % Output on / Output off
        ConnectedLDC%
        Name
        LaserObject       = Dev.LaserObject;
        
        
        
    end
    
    
    
    
    %% TEC specific methods
    % See Folder
    
    
    
    
    
    %% Overloaded GET & SET METHODS
    methods
        

        
        % ############################################ GET METHODS ############################################
        
        
        % Due to security reasons some Values are not allowed to be written to the
        % device (e.g. software limit). Some values also can't be written, because the device
        % produces them itself (e.g. Voltage).
        function obj = set.PeltCurrent(obj,Value)
            
            
            error('Can only be read. To change current, change SetCurrent!')
        end
        
        function obj = set.SLimCurrent(obj,Value)
            error('Can only be read due to safety reasons - Change at device!')
        end
        
        function obj = set.Voltage(obj,Value)
            error('Can only be read. Device property.')
        end
        
        function obj = set.ActTemp(obj,Value)
            error('Can only be read. Device property.')
        end
        
        
        
        
        
        % #################### GET Actual Temperature
        function Answer = get.ActTemp(obj)
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
                if ~strcmp(obj.ThorType,'TEC')
                    error('Thor Device is not a TEC - consider changing the Slot')
                end
            end
            Command = obj.CommandsList{find(strcmp(obj.VariableList,'gActTemp')==1)};
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);           
        end
        
        % #################### GET Peltier Current
        function Answer = get.SetTemp(obj)
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            Command = obj.CommandsList{find(strcmp(obj.VariableList,'gSetTemp')==1)};
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        % #################### GET Peltier Current
        function Answer = get.SLimCurrent(obj)
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            Command = obj.CommandsList{find(strcmp(obj.VariableList,'gSLimCurrent')==1)};
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        
        % #################### GET Peltier Current
        function Answer = get.Output(obj)
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            Command = obj.CommandsList{find(strcmp(obj.VariableList,'gOutput')==1)};
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            if strcmp(DeviceAnswer(1:3),'OFF')
                Answer = false;
            elseif strcmp(DeviceAnswer(1:2),'ON')
                Answer = true;
            else
                error('Output Readout ERROR')
            end
        end
        
        % #################### GET Voltage
        function Answer = get.Voltage(obj)
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            Command = obj.CommandsList{find(strcmp(obj.VariableList,'gVoltage')==1)};
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        % #################### GET Peltier Current
        function Answer = get.PeltCurrent(obj)
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            Command = obj.CommandsList{find(strcmp(obj.VariableList,'gPeltCurrent')==1)};
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        
        
        
        
        % ############################################ SET METHODS ############################################
        
        % #################### SET OUTPUT
        function obj = set.Output(obj,Value)
            % Enables or disables the output
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            
            
            % Check input and convert boolean to Strings
            if and(Value == true,obj.Output == true) % check if already turned on - create warning
                Dev.Util.cprintf([0, 0.5, 0.0],[obj.Name ' ' 'already turned on\n'])
                StrValue = 'ALREADYON';
            elseif and(Value == false,obj.Output == false)
                Dev.Util.cprintf([0, 0.5, 0.0],[obj.Name ' ' 'already turned off - however communicating\n']) % if already
                StrValue = 'OFF';
            elseif Value == true;
                
                StrValue = 'ON';
                if and(Value,~(obj.ActTemp == obj.SetTemp));
                    Dev.Util.cprintf([0,0.5,0],[obj.Name ': For turning on the TEC the Temperature is set to actual Temperature of' num2str(obj.ActTemp) '�C\n'])
                    obj.SetTemp = obj.ActTemp;
                end
            elseif Value == false
                StrValue = 'OFF';
                
                if ~isempty(obj.ConnectedLDC)
                    if obj.ConnectedLDC.Output
                        error('Must switch off LDC before turning off')
                    end
                    obj.SlotIni;
                end
                
            else
                error('Value must be Boolean')
            end
            
            
            if ~strcmp(StrValue,'ALREADYON') % if device is already turned on - cancel communication to save time
                % Communication with device
                if Value == true
                    Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': Switching on requires waiting of 2 seconds - Safety reason.\n'])
                    pause(2)
                end
                
                if Value == false
                    Dev.Util.cprintf([0.8500, 0.3250, 0.0980],[obj.Name ': Switching of requires waiting of 20 seconds - Safety reason.\n'])
                    pause(20)
                end
                PreCommand = obj.CommandsList{find(strcmp(obj.VariableList,'sOutput')==1)};
                Command = [PreCommand ' ' StrValue];
                VisaWrite(obj,Command);
                pause(.5)
                
                % Read out the device to check if it worked. If device is not
                % interlocked output will not turn on and a error message will
                % appear
            end
            if and(obj.Output == false,Value == true)
                error('Check Interlock')
            end
        end
        
        
        
        
        
        % #################### SET Temperature
        function obj = set.SetTemp(obj,Value)
            % Set the desired Current of the device
            if isa(obj,'Dev.ThorProTEC')
                obj.SlotIni; % needed to choose correct slot
            end
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Input must be a number')
            end
            
            % Check so that Max and Min limit are not exceeded
            if isempty(obj.LaserObject.Temp_Min)
                error('Before Setting the Temperature a minimum must be entered')
            elseif isempty(obj.LaserObject.Temp_Max)
                error('Before Setting the Temperature a maximum must be entered')
            end
            
            if Value <= obj.LaserObject.Temp_Min
                error(['Set Value violates Min Temp of Laser. Desired:' ' ' num2str(Value) '�C vs. Limit:' ' ' num2str(obj.LaserObject.Temp_Min) '�C'])
            elseif Value >= obj.LaserObject.Temp_Max
                error(['Set Value violates Max Temp of Laser. Desired:' ' ' num2str(Value) '�C vs. Limit:' ' ' num2str(obj.LaserObject.Temp_Max) '�C'])
            end
            
            % Communication with device
            StrValue = num2str(Value,'%e'); % Convert input to float number with exponential and that to string
            PreCommand = obj.CommandsList{find(strcmp(obj.VariableList,'sSetTemp')==1)};
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj,Command);
        end
    end
    
end


