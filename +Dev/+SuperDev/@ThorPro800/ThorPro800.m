classdef ThorPro800 < Dev.Comm.VisaObject 
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties (Access = private)
        
        %Name
        Name        
    end
    
    properties
        
        %Thor Pro specific PROPOERTIES
        Slot        = 2;
        ThorType   
        

        
    end
    
    
    
    
    
    
    
    
    %% Other THOR LDC specific methods
    methods
        
        function SlotIni(obj)
            % This function sends a command to the ThorPro device in order
            % to choose the right Slot. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct slot.
            StrValue = num2str(obj.Slot);
            PreCommand = ':SLOT';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
        end

        
    end
    
    %% Set Methods
      methods
          
          function Answer = get.ThorType(obj)
%             obj.SlotIni; % needed to choose correct slot
            Command = ':TYPE:ID?';
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
            Answer = str2double(DeviceAnswer);
            switch Answer
                case 223
                Answer = 'TEC';
                case 191
                    Answer = 'LDC';
            end
          end
          
          
          
%           function Answer = get.Slot(obj)
% %             obj.SlotIni; % needed to choose correct slot
%             Command = ':SLOT?';
%             [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,Command); % Communication with device
%             Answer = str2double(DeviceAnswer);
%           end
          
        function obj = set.Slot(obj,Value)
            % Set the desired Current of the laser
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Current Input must be a number')
            elseif ~any(Value==[1 2 3 4 5 6 7 8])
                error('Current Input must be a positive number')
            end
            
            obj.Slot = Value;
           
        end
      end
    
end


