classdef HP8116A < Dev.SuperDev.FunctionGenerator
 % Maximum 
    
    
    
    properties
        
        %VISA PROPERTIES
Frequency
        %         ConnectedTEC
        
    end
    
    
    
    
    properties (Access = private)
        SweepPoints = 631;
    end
    
    
    
    
    
    
    
    
    
    %     %% Private methods
    methods
        
        function obj = HP8116A(~)
            obj.VisaType = 'visa-gpib';
        end
       
       
                
                 function obj = set.Frequency(obj,Value)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            PreCommand = 'FRQ';
            Command = [PreCommand ' ' num2str(Value) 'HZ'];
            VisaWrite(obj,Command);          
                 end
        
                 
                                  function Answer = get.Frequency(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            %             StrValue = obj.Channel;
            Command = 'IFRQ';
            
            Message = VisaRead(obj,Command);   
            
%              SearchString = strrep(Message,'MFREQ ',' '); % Replace "?" of original message by blanc space
            idx = strfind(Message,'MFRQ '); % find the original message (with blanc) in the raw message of the device.
            StringStart = idx + 5; % get the index of it
            DeviceAnswer2 = Message(idx + 4 + 5);
            
            if strcmp(DeviceAnswer2,' ')
                Factor = 1;
            elseif strcmp(DeviceAnswer2,'K')
                Factor = 1000;
            elseif strcmp(DeviceAnswer2,'M')
                Factor = 1e6;
            elseif strcmp(DeviceAnswer2,'m')
                Factor = 1e-3;
            end
            
            DeviceAnswer = str2double(Message(StringStart:StringStart+3)); % Create message variable which contains device answer without original message
            Answer = DeviceAnswer*Factor;
        end
%         

        
        
    end
end







