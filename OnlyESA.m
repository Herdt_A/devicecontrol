% ################## ESA
ESA = Dev.RS_FSC6;
ESA.VisaAdress = 'TCPIP0::130.83.3.194::inst0::INSTR';
ESA = ESA.DevIni;
ESA.Name = 'ESA ICL2183_1012';

% ################## Initialize Folders
FileFolder = 'Mess1';
MeasurementType = 'FrequNoise';





% ################ MULTIMETER
MultiM = Dev.Agilent34401A;
MultiM.VisaAdress = 'GPIB0::23::INSTR';
MultiM = MultiM.DevIni;
MultiM.Name = 'Multimeter ICL2183_13_25';




% ################## ESA
ESA.RBW = 1e4;
ESA.ChangeTO(15);

ESA = ESA.getTrace();
p1 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on');
for i=1:10
    FileName = [num2str(i) '_42.887mA@2.405V'];
    Dev.Util.SafeEsaTrace(ESA,'NoiseEdgeRising',FileName,MeasurementType);
    DetVoltage(i) = MultiM.VoltageDC;
end
dlmwrite('Trace.DCVolt',DetVoltage);

ESA.RBW = 1e6;
ESA.ContOn;
