%% File version
% v1.00 (06.03.2019)_AHerdt



%% Header % Comment if you use in a larger script


instrreset
clear variables
close all
clc

%% Description of measurement


% ### Required Equipment
% ESA
% Current Source
% Multimeter

% ### Possible Determination of
% RIN
% Frequency Noise
% Linewidth via Frequency Noise

% 01. ### Measurement with laser under threshold
% 02. ### Switch on Laser / set it above threshold
% 03. ### Gather ESA-trace and DC-Voltage of detector for each current
% 03.a) # Gather and save ESA-trace for each current
% 03.b) # Gather DC-Voltage of detector for each current
% 03. c)# Safe DC-Voltage Data
% 04. ### Finish (Switch off afterwards)




%% ################### Initialization of devices  ###################






% ################## LDC

LDC =  Dev.ILX_LDX3210;
LDC.VisaAdress = 'GPIB0::2::INSTR';
LDC = LDC.DevIni;
LDC.Name = 'LDC ICL2183_13_25';



% ################ MULTIMETER
MultiM = Dev.Agilent34401A;
MultiM.VisaAdress = 'GPIB0::11::INSTR';
MultiM = MultiM.DevIni;
MultiM.Name = 'Multimeter ICL2183_13_25';


% ################ LockIn
LockInAmp = Dev.SR530LockIn;
LockInAmp.VisaAdress = 'GPIB0::23::INSTR';
LockInAmp = LockInAmp.DevIni;
LockInAmp.Name = 'LockInAmp ICL2183_13_25';


%% ################### Initialization of Folders  ###################

% ################## Initialize Folders
FileFolder = 'Measurement2';
MeasurementType = 'Kennlinie';





%% ################## SET DEVICE PARAMETERS ##################

% ################## LASER
Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;




% ################## LDC
% Set Laserlimits and so on
LDC.LaserObject = Laser_ICL2183_10_12;
LDC.SLimCurrent = .05;
SafeCurrent = 0.002;

% Set Stepsizes
StartCurrent = 0.001;
EndCurrent   = 0.0490;
StepSizeCurrent = 0.0001;









%% ################## Measurement ##################
if LDC.Output == true;
    LDC.goSafe2Current(SafeCurrent);
end





% ################## 02. ### Switch on Laser / set it above threshold

LDC.Output = true;
LDC.goSafe2Current(StartCurrent);



% ################## 03. ### Gather ESA-trace and DC-Voltage of detector for each current



NumSteps = round((EndCurrent - StartCurrent)/StepSizeCurrent + 1);

for i=1:NumSteps;
    % ################## 03.a) # Gather and save ESA-trace for each current
    
    
    NextCurrent =StartCurrent+StepSizeCurrent*(i-1);
    CurrentList(i) = NextCurrent;
    
    disp(['Next Current =' ' ' num2str(NextCurrent*1000) 'mA'])
    
    LDC.SetCurrent = NextCurrent;
    pause(0.2)
    CurrentActList(i) = LDC.ActCurrent;
    
    pause(LockInAmp.TimeAverage*1.1)
    
  
    % ################## 03.b) # Gather DC-Voltage of detector for each current
    
    
    DetVoltage(i) = LockInAmp.OutputAmp;
    
    % ### Monitoring Plot 3
        figure(1)
        p1 = plot(CurrentActList*1000,DetVoltage,'Color',[0 0.447 0.741],'LineSmoothing','on','Linewidth',2);
        xlabel('real current [mA]')
        ylabel('Multimeter Voltage [V]')
        set(gca,'FontSize',14)
        set(gca,'FontName','Times New Roman')
        
end

name= '1-50mA,100ms';

% ################## 03. c)# Safe DC-Voltage Data
Dev.Util.SafeToAsci([CurrentList' DetVoltage'],FileFolder,name,MeasurementType);


% ################## 04. ### Finish (Switch off afterwards)



LDC.goSafe2Current(SafeCurrent-0.001);
% LDC.switchOff();

