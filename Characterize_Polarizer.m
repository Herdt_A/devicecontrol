%% Header % Comment if you use in a larger script

instrreset
clear variables
close all
clc

%% ################### Initialization





M1 = motor;
M1Adress = '55937830';
M1.connect(M1Adress);


MM = Dev.Agilent34401A;
MM.VisaAdress = 'GPIB0::11::INSTR';
MM = MM.DevIni;
MM.Name = 'Multimeter';




%% ################## SET DEVICE PARAMETERS ##################
M1.home;
% Set Motor Stepsizes
StartAngle      = 0;
EndAngle        = 360;
StepSizeAngle   = 0.5;

%% ################## Read Out



NumMotorSteps = round((EndAngle - StartAngle)/StepSizeAngle + 1);


% ################## 02. ### Gather ESA Signal for each Frequency and
% current using the lock in

for ii =1:NumMotorSteps
    
    NextAngle =StartAngle+StepSizeAngle*(ii-1);
    AngleList(ii) = NextAngle;
    M1.moveto(NextAngle);
    
    pause(.2);
    MMVoltage(ii) = MM.VoltageDC;
    pause(.1);
    
    plot(AngleList,MMVoltage,'LineWidth',2);
    drawnow;
    xlabel('Angle in Deg')
    ylabel('Voltage in V')
    set(gca,'FontSize',14);
    
    
end
Dev.Util.SafeToAsci([AngleList' MMVoltage'],'PolCharacterization','VoltageVsAngle')