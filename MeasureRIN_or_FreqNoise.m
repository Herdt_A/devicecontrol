%% File version
% v1.00 (27.02.2019)_AHerdt



%% Header % Comment if you use in a larger script


instrreset
clear variables
close all
clc

%% Description of measurement


% ### Required Equipment
% ESA
% Current Source
% Multimeter

% ### Possible Determination of
% RIN
% Frequency Noise
% Linewidth via Frequency Noise

% 01. ### Measurement with laser under threshold
% 02. ### Switch on Laser / set it above threshold
% 03. ### Gather ESA-trace and DC-Voltage of detector for each current
% 03.a) # Gather and save ESA-trace for each current
% 03.b) # Gather DC-Voltage of detector for each current
% 03. c)# Safe DC-Voltage Data
% 04. ### Finish (Switch off afterwards)




%% ################### Initialization of devices  ###################



% ################## ESA
ESA = Dev.RS_FSC6;
ESA.VisaAdress = 'TCPIP0::130.83.3.194::inst0::INSTR';
ESA = ESA.DevIni;
ESA.Name = 'ESA ICL2183_1012';


% ################## LDC

LDC =  Dev.ILX_LDX3210;
LDC.VisaAdress = 'GPIB0::3::INSTR';
LDC = LDC.DevIni;
LDC.Name = 'LDC ICL2183_13_25';



% ################ MULTIMETER
MultiM = Dev.Agilent34401A;
MultiM.VisaAdress = 'GPIB0::23::INSTR';
MultiM = MultiM.DevIni;
MultiM.Name = 'Multimeter ICL2183_13_25';


%% ################### Initialization of Folders  ###################

% ################## Initialize Folders
<<<<<<< HEAD:MeasureRIN.m
FileFolder = 'Measurement1a';
MeasurementType = 'FrequNoise';
=======
FileFolder = 'Measurement1';
MeasurementType = 'RIN';
>>>>>>> 81343ee050af68415c068cee2425fed5b60b322c:MeasureRIN_or_FreqNoise.m





%% ################## SET DEVICE PARAMETERS ##################

% ################## LASER
Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;


% ################## ESA
ESA.RBW = 1e4;
ESA.ChangeTO(15);

% ################## LDC
% Set Laserlimits and so on
LDC.LaserObject = Laser_ICL2183_10_12;
LDC.SLimCurrent = .05;
SafeCurrent = 0.020;

% Set Stepsizes
StartCurrent = 0.042;
EndCurrent   = 0.043;
StepSizeCurrent = 0.0001;









%% ################## Measurement ##################
if LDC.Output == true;
    LDC.goSafe2Current(SafeCurrent);
end



% ################## 01. ### Measurement with laser under threshold
ESA = ESA.getTrace;
DetVoltage(1) = MultiM.VoltageDC;
name = 'DetectorNoise';
p1 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on');
Trace1 = ESA.Trace;
l1 = legend(name);
xlabel('frequency [MHz]')
ylabel('Spectral intensity [dBm]')
set(gca,'FontSize',12)
Dev.Util.SafeEsaTrace(ESA,FileFolder,name,MeasurementType)

CurrentList(1) = SafeCurrent;
CurrentActList(1) = LDC.ActCurrent;



% ################## 02. ### Switch on Laser / set it above threshold

LDC.Output = true;
<<<<<<< HEAD:MeasureRIN.m

StartCurrent = 0.0424;
=======
>>>>>>> 81343ee050af68415c068cee2425fed5b60b322c:MeasureRIN_or_FreqNoise.m
LDC.goSafe2Current(StartCurrent);



% ################## 03. ### Gather ESA-trace and DC-Voltage of detector for each current


<<<<<<< HEAD:MeasureRIN.m
for i=1:101
    % ################## 03.a) # Gather ESA-trace for each current
    
    
    NexCurrent =StartCurrent+0.00001*(i-1);
=======


NumSteps = round((EndCurrent - StartCurrent)/StepSizeCurrent + 1);

for i=1:NumSteps;
    % ################## 03.a) # Gather and save ESA-trace for each current
    
    
    NexCurrent =StartCurrent+StepSizeCurrent*(i-1);
>>>>>>> 81343ee050af68415c068cee2425fed5b60b322c:MeasureRIN_or_FreqNoise.m
    CurrentList(i+1) = NexCurrent;
    
    disp(['Next Current =' ' ' num2str(NexCurrent*1000) 'mA'])
    LDC.SetCurrent = NexCurrent;
    CurrentActList(i+1) = LDC.ActCurrent;
    ESA = ESA.getTrace;
<<<<<<< HEAD:MeasureRIN.m
    name = [num2str(LDC.ActCurrent*1000,'%.2f') 'mA'];
=======
    name = [num2str(round(NexCurrent*1000,2),'%07.3f') 'mA'];
    
    
    % ### Monitoring Plot 1
>>>>>>> 81343ee050af68415c068cee2425fed5b60b322c:MeasureRIN_or_FreqNoise.m
    figure(1)
    p1 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on','Linewidth',2);
    l1 = legend(name);
    xlabel('frequency [MHz]')
    ylabel('Spectral intensity [dBm]')
    set(gca,'FontSize',12)
    Dev.Util.SafeEsaTrace(ESA,FileFolder,name,MeasurementType)
    
    % ### Monitoring Plot 2
    figure(2)
    p2 = plot(ESA.Trace(:,1)/1e6,ESA.Trace(:,2)-Trace1(:,2),'Color',[0 0.447 0.741 .5],'LineSmoothing','on','Linewidth',2);
    l2 = legend(name);
    xlabel('frequency [MHz]')
    ylabel('Spectral intensity - Ref[dBm]')
    set(gca,'FontSize',12)
    
    % ################## 03.b) # Gather DC-Voltage of detector for each current
    
    
    DetVoltage(i+1) = MultiM.VoltageDC;
<<<<<<< HEAD:MeasureRIN.m
    figure(3)
    if i>2
    p3 = plot(CurrentList(2:end)*1000,DetVoltage(2:end),'Color',[0 0.447 0.741],'LineSmoothing','on','Linewidth',2);
    l3 = legend(name);
    xlabel('current [mA]')
    ylabel('detector voltage[dBm]')
    set(gca,'FontSize',12)
=======
    
    % ### Monitoring Plot 3
    if i>1
        figure(3)
        p3 = plot(CurrentActList(2:end)*1000,DetVoltage(2:end),'Color',[0 0.447 0.741],'LineSmoothing','on','Linewidth',2);
        l3 = legend(name);
        xlabel('real current [mA]')
        ylabel('detector voltage[dBm]')
        set(gca,'FontSize',12)
>>>>>>> 81343ee050af68415c068cee2425fed5b60b322c:MeasureRIN_or_FreqNoise.m
    end
    pause(1)
end

<<<<<<< HEAD:MeasureRIN.m

dlmwrite('Traceabc.DCVolt',[CurrentList' DetVoltage']);
=======
% ################## 03. c)# Safe DC-Voltage Data
Dev.Util.SafeToAsci([CurrentList CurrentActList DetVoltage],FileFolder,name,MeasurementType);

>>>>>>> 81343ee050af68415c068cee2425fed5b60b322c:MeasureRIN_or_FreqNoise.m

% ################## 04. ### Finish (Switch off afterwards)


% Switch Off / go below threshold
ESA.RBW = 1e6;
ESA.ContOn;
LDC.goSafe2Current(SafeCurrent-0.002);
% LDC.switchOff();

