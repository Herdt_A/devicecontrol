function obj = CreateVisa(obj)
% Open Communication channel if device has not already been
% initialized
if isempty(obj.VisaObject)
    visa('ni',obj.VisaAdress);
    if ~strcmp(obj.VisaAdress(1:3),'USB')
        obj.VisaObject = instrfind('Type', 'visa-gpib', 'RsrcName', obj.VisaAdress, 'Tag', '');
    else
        obj.VisaObject = instrfind('Type', 'visa-usb', 'RsrcName', obj.VisaAdress, 'Tag', '');
    end
    % Display Error VISA-GPIB object if it does not exist
    if isempty(obj.VisaObject)
        error('No Object Found')
    else
        fclose(obj.VisaObject);
        obj.VisaObject = obj.VisaObject(1);
    end
    fopen(obj.VisaObject);
else
    warning('Device already initialized')
end

end