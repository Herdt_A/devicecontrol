        function obj = InitializeDevice(obj)
            % Function to initalize the 
            if isempty(obj.VisaObject)
                obj = CreateVisa(obj);
                disp(strcat(obj.Name,{' '},'sucessfully initialized.'))
            else
                warning('Device already initialized')
            end
        end