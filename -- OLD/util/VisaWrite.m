        function VisaWrite(obj,Message)
            % Check if Input is correct
            if ~ischar(Message)
                error('message has to be of type char')
            end
            
            % Transfer to device device for Value
            fprintf(obj.VisaObject, Message);
            
        end