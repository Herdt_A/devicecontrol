function obj1 = OpenVisa(InputAdress)

obj1 = instrfind('Type', 'visa-gpib', 'RsrcName', InputAdress, 'Tag', '');
% Create the VISA-GPIB object if it does not exist
% otherwise use the object that was found.
if isempty(obj1)
% obj1 = visa('NI', 'GPIB0::2::INSTR');
error('No Object Found')
else
fclose(obj1);
obj1 = obj1(1);
end
fopen(obj1);

end