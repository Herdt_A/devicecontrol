classdef LDX3232
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        
        %VISA PROPERTIES
        VisaAdress  = 'GPIB0::1::INSTR'
        VisaObject

        
        %LDC specific PROPERTIES
        ActCurrent    % actual current of the Laser
        SetCurrent    % set current
        SLimCurrent   % software limit of current
        HLimCurrent   % hardware limit of current
        Output       % Output on / Output off
        Voltage      % Voltage of the device
        AssLaser    = 'Laser1' % Assosiated Laser
        Name        = 'LDC1'
        ConnectedTEC
        
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    %% Private methods
%     methods (Access = private)
%         function SlotIni(obj)
%             % This function sends a command to the ThorPro device in order
%             % to choose the right Slot. It is necessary to include this
%             % function before every set or get command to assure that it
%             % gets data from the correct slot.
%             StrValue = num2str(obj.Slot);
%             PreCommand = ':SLOT';
%             Command = [PreCommand ' ' StrValue];
%             VisaWrite(obj, Command);
%         end
%     end
    
    
    
    
    
    
    
    
    
    
    %% Other THOR LDC specific methods
    methods
        
        function obj = Initialize(obj)
            % Initializes the object by calling the InitializeDevice
            % function
            obj = InitializeDevice(obj);
        end
        
        
        
        
        
        function [varargout] = goSafe2Current(obj,Value,varargin)
            % goSafe2Current(obj,EndCurrent,float(Speed),boolean(ShowSteps))
            % Increases or decreases the Current to EndCurrent.
            % Speed is a number and is given in mA per second.
            % Standard Speed is 2mA per second
            % ShowSteps is true or false and visualizes the steps.
            
            
            % Safety checks
            
            % Only starts GoSafe2Current if output is turned on
            if obj.Output == false
                error('Output turned off. To go safe to current, turn output on!')
            end
            
            % Only starts GoSafe2Current if Hardwarelimit and Softwarelimit
            % are not exceeded.
            if Value > obj.HLimCurrent
                error('Final goSafe2Current Value violates Hardwarelimit')
            elseif Value > obj.SLimCurrent
                error('Final goSafe2Current Value violates Softwarelimit')
            end
            
            % Precalculations
            Difference = Value - obj.ActCurrent; % Difference between recent current and EndCurrent
            StartCurrent = obj.ActCurrent; % Safe recent current in variable
            
            
            % Initialze Values
            StepSize = 0.001; % 1mA Steps
            Show = false; % Visualizer of acutal output
            
            % Create Speed Value if no input is given
            if isempty(varargin)
                Speed = 2; % Definition of Standard Speed
            else
                Speed = varargin{1}; % Update Speed if required
                if length(varargin) >= 2
                    Show = varargin{2}; % Update Visualizer if required
                end
            end
            
            
            
            % Communication speed is limited by ThorPro. To increase speed
            % the stepsize has to be updated to higher values. Maximum
            % current per second is
            if Speed <= 4
            elseif and(Speed > 4,Speed<=8);
                disp('goSafe2Current increased Stepsize to 2mA')
                StepSize = 0.002; % 2mA steps
                Speed = Speed/2; % Devide speed again
            elseif and(Speed > 8,Speed<=16);
                disp('goSafe2Current increased Stepsize to 4mA')
                StepSize = 0.004; % 2mA steps
                Speed = Speed/4;
            elseif and(Speed > 16,Speed<=32);
                disp('goSafe2Current increased Stepsize to 8mA')
                StepSize = 0.008; % 2mA steps
                Speed = Speed/8;
            else
                error('goSafe2Current cant go faster than 32mA per second! StepSize max = 8mA due to safety reasons')
            end
            
            
            StepNumber = floor(Difference / StepSize); % Calculate Stepnumber
            
            
            
            % Create Waitbar handle or create "warning message" in the case
            % the set current = actual current
            if ~(StepNumber == 0)
                %                 disp([obj.Name ': ' 'goSafe2Current from:' ' ' num2str(round(obj.ActCurrent*1000),2) 'mA to' ' ' num2str(Value*1000) 'mA'])
                f = waitbar(0,[obj.Name ': ' 'Go to current ' num2str(Value*1000) 'mA: ' num2str(round(obj.ActCurrent*1000),2) 'mA']);
            else
                disp('goSafe2Current failed: Set Current = Act Current')
            end
            
            
            % Reverse Stepsize is SetCurrent < Actcurrent to obtain
            % positive stepnumber
            if StepNumber < 0;
                StepSize = -StepSize;
                StepNumber = -StepNumber;
            end
            
            
            % Initialize Output Current and Output Voltage, which are
            % arrays that track Voltage and Current of the device during
            % the goSafe2Current process
            OutCurrent(1:floor(Value)) = nan;
            OutVoltage(1:floor(Value)) = nan;
            LoopTime(1:floor(Value)) = nan;
            TimeStart = tic;
            
            % Actual current iteration
            for i=1:floor(StepNumber)
                waitbar(i/(floor(StepNumber)+1),f,[obj.Name ': ' 'Go to current ' num2str(Value*1000) 'mA: ' num2str(round(obj.ActCurrent*1000),'%3.1f') 'mA']);    % update Waitbar
                
                % Create variable that measures the time it takes for a
                % readout
                ReadOutTimeStart = tic;
                
                
                % Iterator
                obj.SetCurrent = StartCurrent + i*StepSize;
                
                % Display the actual current in the console, if varargin
                if Show
                    disp([num2str(obj.ActCurrent*1000) ' ' 'mA'])
                    disp([num2str(obj.Voltage) ' ' 'V'])
                end
                
                
                OutCurrent(i) = obj.ActCurrent; % Update Current Array
                OutVoltage(i) = obj.Voltage; % Update Voltage Array
                LoopTime(i) = toc(TimeStart); % Update Time Array
                
                ReadOutTime = toc(ReadOutTimeStart); % Get time it need to read out the device
                
                pause(1/Speed-ReadOutTime) % pause the right time. 1/Speed is the time in seconds it should take for a loop in order to achieve the right speed, but you have to substract the ReadOutTime of the device
                
            end
            
            
            % After the Loop is finished set the Current value the input
            % Value and substract LoopTime, so that Looptime(1) = 0.
            obj.SetCurrent = Value;
            LoopTime = LoopTime - LoopTime(1);
            
            
            % This is for deleting the Waitbar
            if ~(StepNumber ==0) % Needed to check because delete(f) doesnt work if not
                waitbar(1,f,['Go to current ' num2str(Value*1000) 'mA: ' num2str(round(obj.ActCurrent*1000),'%3.1f') 'mA']);  % 100%
                pause(.5) % Wait so that you can see 100%
                delete(f) % Close the waitbar
            end
            
            
            % If varargin Show  == true then show the actual current.
            if Show
                disp([num2str(obj.ActCurrent*1000) ' ' 'mA'])
            end
            
            % Concatenate Time, Current and Voltage in one Array
            varargout{1} = [LoopTime' OutCurrent' OutVoltage'];
            
            
        end
        
        
        
        
        
        
        
        
        function switchOff(obj)
            % switchOff(ThorLDCObject)
            % Switches ThorLDC safely off by turning the current slowly
            % down.
            
            % Check if output = on or off. If output is already off do
            % nothing.
            if obj.Output == true
                obj.goSafe2Current(0); % go slowly to 0mA
                % Wait 2 Seconds to Switch off
                disp([obj.Name ' ' 'switches off in 2s']) 
                pause(2)
                % Switch off and display success
                obj.Output = false;
                disp([obj.Name ' ' 'successfully switched off'])
            else
                disp([obj.Name ' ' 'already switched off'])
            end
        end
        
    end
    
    
    
    
    
    
    
    
    
    %% GET methods
    methods
        
        
        function Answer = get.Voltage(obj)
            % Visa Read Voltage
%             obj.SlotIni; % needed to choose correct slot
            DeviceAnswerRaw = VisaRead(obj,':LAS:LDV?'); % Communication with device
            Answer = str2double(DeviceAnswerRaw(1:end-1));
        end
        
        function Answer = get.ActCurrent(obj)
            % Visa Read actual current
%             obj.SlotIni; % needed to choose correct slot
            DeviceAnswerRaw = VisaRead(obj,':LAS:LDI?'); % Communication with device
            Answer = str2double(DeviceAnswerRaw(1:end-1))/1000;
        end
        
        function Answer = get.SLimCurrent(obj)
            % Visa Read software limit of current
%             obj.SlotIni; % needed to choose correct slot
            DeviceAnswerRaw = VisaRead(obj,':LAS:LIM:I1?'); % Communication with device
            Answer = str2double(DeviceAnswerRaw(1:end-1))/1000;
        end
        
        
        function Answer = get.HLimCurrent(obj)
            % Visa Read hardware limit of current
%             obj.SlotIni; % needed to choose correct slot
            DeviceAnswerRaw = VisaRead(obj,':LAS:LIM:I1?'); % Communication with device
            Answer = str2double(DeviceAnswerRaw(1:end-1))/1000;
        end
        
        
        function Answer = get.Output(obj)
            % Visa Read if power output is on or off
%             obj.SlotIni; % needed to choose correct slot
            DeviceAnswerRaw = VisaRead(obj,':LAS:OUT?'); % Communication with device
            
            % Convert answer to boolean
            if strcmp(DeviceAnswerRaw(1),'0')
                Answer = false;
            elseif strcmp(DeviceAnswerRaw(1),'1')
                Answer = true;
            else
                error('Output Readout ERROR')
            end
            
        end
        
        
        function Answer = get.SetCurrent(obj)
            % Visa Read the chosen current
%             obj.SlotIni; % needed to choose correct slot
            DeviceAnswerRaw = VisaRead(obj,':LAS:SET:LDI?'); % Communication with device
            Answer = str2double(DeviceAnswerRaw(1:end-1))/1000;
        end
        
%         function Answer = get.LDPol(obj)
%             obj.SlotIni; % needed to choose correct slot
%             [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':LAS:MODE?'); % Communication with device
%             
%             % Convert Answer to String
%             if strcmp(DeviceAnswer(1:2),'CG')
%                 Answer = 'CG';
%             elseif strcmp(DeviceAnswer(1:2),'AG')
%                 Answer = 'AG';
%             else
%                 error('Output Readout ERROR')
%             end
%             
%         end
    end
    
    
    
    
    
    
    
    %% SET methods
    methods
        
%         
%         function obj = set.Slot(obj,Value)
%             % Set the slot
%             %             addpath('../../../Functions')
%             
%             % Check input
%             Flag1 = and(isfloat(Value),Value == floor(Value)); % It must be an integer
%             Flag2 = Value <= 8 & Value >= 1; % It must be between 1 and 8
%             if and(Flag1,Flag2)
%                 obj.Slot = Value; % Set the value
%             else
%                 error('Slot must be integer between 1 and 8 ERROR#1')
%             end
%             
%         end
        
        
        
        function obj = set.SetCurrent(obj,Value)
            % Set the desired Current of the device
%             obj.SlotIni; % needed to choose correct slot
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Current Input must be a number')
            end
            
            % Check so that Hardware and Software limit are not exceeded
            if Value >= obj.HLimCurrent
                error('Set Value violates Hardware Limit!!! Remember: Current Input is in A!')
            elseif Value >= obj.SLimCurrent
                error('Set Value violates Software Limit!!! Remember: Current Input is in A!')
            end
            
            % Communication with device
            StrValue = num2str(Value*1000,'%e'); % Convert input to float number with exponential and that to string
            PreCommand = ':LAS:LDI';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj,Command);
        end
        
        
        
        
        function obj = set.Output(obj,Value)
            % Enables or disables the output
%             obj.SlotIni; % needed to choose correct slot
            
            
            % Check input and convert Output-boolean to String depending on
            % the recent state of the output.
            if and(Value == true,obj.Output == true) % Desire: Turn on. Check if already turned on
                disp([obj.Name ' ' 'already turned on']) % create warning if already turned on
                StrValue = 'ALREADYON';
                
            elseif and(Value == false,obj.Output == false) % Desire: Turn off. Check if already turned off, but send the command anyway.
                disp([obj.Name ' ' 'already turned off - however communicating'])  % Create Warning
                StrValue = '0';
                
            elseif and(Value == true,obj.Output == false); % Desire: Turn on. Check if turned off.
                
                % Set String to send to "on"
                StrValue = '1';
                
                % If there is a connceted TEC, check if the output of it is
                % enabeled, otherwise create warning
                if ~isempty(obj.ConnectedTEC)
                    if obj.ConnectedTEC.Output == false
                        error('TEC must be turned on to turn on laser')
                    end
                end
                
                
                % Set SetCurrent to 0 in order to prevent that the current
                % is very high when the laser is turned on
                if Value;
                    disp([obj.Name ':' 'For turning on the Current is set to 0mA'])
                    obj.SetCurrent = 0;
                end
                
                
            elseif and(Value == false,obj.Output == true) % Desire: Turn off. Check if turned on.
                % Set String to send to "off"
                StrValue = '0';
                
            else
                error('goSafe2Current ERROR #1')
            end
            
            % Communication with device
            if ~strcmp(StrValue,'ALREADYON') % if device is already turned on - cancel communication to save time
                
                if Value == true
                    disp([obj.Name ':' 'Switching on requires waiting of 2 seconds - Safety reason.'])
                    pause(2)
                end
                
                % Send String to Device
                PreCommand = ':LAS:OUT';
                Command = [PreCommand ' ' StrValue];
                VisaWrite(obj,Command);
                pause(.5)
                
                % Read out the device to check if it worked. If device is not
                % interlocked output will not turn on and a error message will
                % appear
            end
            
            % The controller 
            if and(obj.Output == false,Value == true)
                error('Check Interlock')
            end
        end
        
        
        
        
        
        % Due to security reasons some Values are not allowed to be written to the
        % device (e.g. software limit). Some values also can't be written, because the device
        % produces them itself (e.g. Voltage).
        function obj = set.ActCurrent(obj,Value)
            error('Can only be read. To change current, change SetCurrent!')
        end
        
        function obj = set.HLimCurrent(obj,Value)
            error('Can only be read due to safety reasons - Change at device!')
        end
        
        function obj = set.SLimCurrent(obj,Value)
            error('Can only be read due to safety reasons - Change at device!')
        end
        
        function obj = set.Voltage(obj,Value)
            error('Can only be read. Device property.')
        end
        
        
        
        
        
        
        
    end
    
end


