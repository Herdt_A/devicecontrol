classdef (Abstract) Regler
    properties
        
        %VISA PROPERTIES
        VisaAdress
        VisaObject
        
        %TEC specific PROPERTIES
        IstWert % actual Temperature current of the TEC
        SollWert % set Temperature current of the TEC
        MinTemp % minimal temperature
        MaxTemp % maximal temperature
        PeltCurrent    % actual Peltier current of the TEC
        SLimCurrent   % Software limit of Peltier current
        HLimCurrent   % Hardware limit of current
        Output       % Output on / Output off
        Voltage      % Voltage of the device
        AssLaser    = 'Laser1' % Assosiated Laser
        Name        = 'TEC1'
        ConnectedLDC%
        
    end
end