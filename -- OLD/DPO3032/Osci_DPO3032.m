classdef Osci_DPO3032
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        
        %VISA PROPERTIES
        VisaAdress  = 'USB0::0x0699::0x0412::C010898::0::INSTR'
        VisaObject
        
        %DPO3032 Pro specific PROPOERTIES
        Channel        = 'CH1';
        
        %LDC specific PROPERTIES
        Trace
        
        %         ActCurrent    % actual current of the Laser
        %         SetCurrent    % set current
        %         SLimCurrent   % software limit of current
        %         HLimCurrent   % hardware limit of current
        %         Output       % Output on / Output off
        %         Voltage      % Voltage of the device
        %         LDPol        % Pol
        %         AssLaser    = 'Laser1' % Assosiated Laser
        Name        = 'Osci1'
        %         ConnectedTEC
        
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    %     %% Private methods
    methods (Access = private)
        
        
        
        
        function ChannelIni(obj)
            % This function sends a command to the DPO 3032 device in order
            % to choose the right trace. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct Channel.
            StrValue = obj.Channel;
            PreCommand = ':DATA:SOURCE';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
        end
        
        function OutputFormatIni(obj)
            % This function initializes that the output format is formatted
            % in the right way
            
            % Sets the output to be binary
            StrValue = 'RIB';
            PreCommand = ':DAT:ENC';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
            
            % Sets the output to be a 2byte integer
            BytePerValue = 2;
            StrValue = num2str(BytePerValue);
            PreCommand = ':WFMO:BYT_N';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
        end
    end
    
    
    
    
    
    
    
    
    
    
    %% Other THOR LDC specific methods
    methods
        
        function obj = Initialize(obj)
            % Initializes the object by calling the InitializeDevice
            % function
            obj = InitializeDevice(obj);
        end
        
        
        
        
    end
    
    
    
    
    
    
    %
    %
    %
    %% GET methods
    methods
        
        
        function Answer = get.Trace(obj)
            % Visa Read Voltage
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,'ID?'); % Communication with device
            Answer = DeviceAnswerRaw;
            
        end
        
        function Answer = Test(obj)
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,'*WAI;:WFMPRE:YOFF?;:WFMPRE:YMULT?;:WFMPRE:YZERO?;:WFMPRE:XINCR?;:WFMPRE:XZERO?;:DAT:WID?'); % Communication with device
            Answer = DeviceAnswerRaw;
        end
        
        
        function [Answer,varargout] = getTrace(obj,StartPoint,StopPoint,varargin)
            Show = false;
            if ~isempty(varargin)
                Show = varargin{1};
            end
            obj.ChannelIni;
            obj.OutputFormatIni;
            
            StrValue = num2str(StartPoint);
            PreCommand = 'DAT:STAR';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
            
            
            StrValue = num2str(StopPoint);
            PreCommand = 'DAT:STOP';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
            
            VisaRead(obj,'DAT:STAR?');
            HorizontalRecordLength = str2double(VisaRead(obj,'HOR:RECO?'));
            
            if HorizontalRecordLength < StopPoint-StartPoint+1;
                disp(['Read out length set to: ' num2str(HorizontalRecordLength) ' Datapoints.'])
            end
            
            NumberOfBytes = min((StopPoint - StartPoint)*2,HorizontalRecordLength);
            
            Devider = 1;
            if NumberOfBytes/2<=10000
                Devider = 64;
            elseif NumberOfBytes/2<=100000
                Devider = 4;
            elseif NumberOfBytes/2<=1000000
                Devider = 1;
            end
            %             VisaRead(obj,':WFMO:BYT_N?');
            %             VisaRead(obj,':DAT:ENC?');
            
            %             VisaRead(obj,':DAT:ENC?');
            
            %             VisaRead(obj,'*WAI')
            yoff = str2double(VisaRead(obj,':WFMPRE:YOFF?'));
            ymult = str2double(VisaRead(obj,':WFMPRE:YMULT?'));
            yzero = str2double(VisaRead(obj,':WFMPRE:YZERO?'));
            xincr = str2double(VisaRead(obj,':WFMPRE:XINCR?'));
            %             xzero = str2num(VisaRead(obj,':WFMPRE:XZERO?'));
            %             wid = VisaRead(obj,':DAT:WID?')
            
            
            readasync(obj.VisaObject);
            %             Value1 = obj.VisaObject.InputBufferSize;
            %             Value2 = obj.VisaObject.OutputBufferSize;
            
            
            fclose(obj.VisaObject);
            obj.VisaObject.InputBufferSize =  1024*256/Devider;
            obj.VisaObject.OutputBufferSize = 1024*256/Devider;
            NumberOfIterations = ceil(NumberOfBytes/(1024*256/Devider));
            fopen(obj.VisaObject);
            
            pause(.01)
            
            StopIteration = false;
            i =0;
            Data = [];
            VisaWrite(obj,'CURVE?');
            
            %             Show = true;
            
            if Show
                f = waitbar(0,[obj.Name ': Read Out:' num2str(0) '%']);
            end
            
            while ~StopIteration
                
                
                
                %                 pause(.1)
                i = i+1;
                if Show
                    waitbar(i/NumberOfIterations,f,[obj.Name ': Read Out:' num2str(round(i/NumberOfIterations*100)) '%'])
                end
                %                 VisaRead{'BUSY?'}
                DataArray{i} = fread(obj.VisaObject, floor(obj.VisaObject.InputBufferSize) , 'uint8');
                length(DataArray{i});
                %                 pause(0.1)
                StopIteration = length(DataArray{i}) < floor(obj.VisaObject.InputBufferSize);
                Data = [Data;DataArray{i}];
            end
            
            
            if Show
                waitbar(1,f,[obj.Name ': Read Out:' num2str(100) '%'])
                pause(.5)
                delete(f)
            end
            
            
            if cast(Data(1),'char')=='#'
                CheckBinary = true;
                CheckNumber = str2num(cast(Data(2),'char'));
                ByteNumber =  str2num(cast(Data(3:3+CheckNumber-1),'char')');
            else
                error('Data not in binary mode')
            end
            
            
            headerdata =  cast(Data(1:3+CheckNumber-1),'char');
            Answer(:,3) = cast(swapbytes(typecast(cast(Data(3+CheckNumber:end-1),'uint8'),'int16')),'double');
            Answer(:,2) = (Answer(:,3) - yoff)*ymult + yzero;
            Answer(:,1) =  (0:xincr:xincr*(length(Answer)-1))';
            varargout = headerdata';
            
            if ~(length(Answer) == ByteNumber/2);
                warning('buggy')
            end
        end
        
    end
    
    
    
    
    
    
    
    
    
    
    %% SET METHODS
    methods
        
        %
        function obj = set.Channel(obj,Value)
            % Set the slot
            addpath('../../Functions')
            
            % Check input
            Flag1 = ischar(Value); % It must be an string
            
            Flag2 = any(strcmp(Value,{'CH1','CH2','REF1','REF2','REF3','REF4','MATH'}));
            if and(Flag1,Flag2)
                obj.Channel = Value; % Set the value
            else
                error('Channel must be string containing CH1, CH2, REF1, REF2, REF3, REF4, MATH  ERROR#1')
            end
            
        end
    end
   
end


