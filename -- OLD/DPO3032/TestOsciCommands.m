clear all
close all
addpath('util')


Osci1 = Osci_DPO3032;
Osci1 = Osci1.Initialize;
Osci1.Channel = 'CH1';


RootFolder = 'C:\Users\HLO\Documents\Messungen_Albert\MatlabMessung';
cd(RootFolder)
FileFolder = 'MessreiheNDF';
FileName = '100NDF';
mkdir(FileFolder);


for Iteration = 1:25
Answer = Osci1.getTrace(1,100000,false);


Answer2(:,1) = Answer(:,1);
Answer2(:,Iteration+1) = Answer(:,2);


disp(Iteration)
end


filename = strcat(RootFolder,'\',FileFolder,'\',FileName,'.mat');
save(filename,'Answer2')
fclose(Osci1.VisaObject);
delete(Osci1.VisaObject);
instrreset

p1 = plot(Answer2(:,2:end),'-','LineSmoothing','on','LineWidth',0.1);

for i=1:25
p1(i).Color(4)=0.25;
end