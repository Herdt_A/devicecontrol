classdef ThorTEC
    %THORLDC Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    properties
        
        %VISA PROPERTIES
        VisaAdress  = 'GPIB0::1::INSTR'
        VisaObject
        
        %Thor Pro specific PROPOERTIES
        Slot        = 2;
        
        %TEC specific PROPERTIES
        ActTemp % actual Temperature current of the TEC
        SetTemp % set Temperature current of the TEC
        MinTemp % minimal temperature
        MaxTemp % maximal temperature
        PeltCurrent    % actual Peltier current of the TEC
        SLimCurrent   % Software limit of Peltier current
        HLimCurrent   % Hardware limit of current
        Output       % Output on / Output off
        Voltage      % Voltage of the device
        AssLaser    = 'Laser1' % Assosiated Laser
        Name        = 'TEC1'
        ConnectedLDC%
        
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    %% Private methods
    methods (Access = private)
        function SlotIni(obj)
            % This function sends a command to the ThorPro device in order
            % to choose the right Slot. It is necessary to include this
            % function before every set or get command to assure that it
            % gets data from the correct slot.
            StrValue = num2str(obj.Slot);
            PreCommand = ':SLOT';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj, Command);
        end
    end
    
    
    
    
    
    
    
    
    
    
    %% Other THOR LDC specific methods
    methods
        
        function obj = Initialize(obj)
            % Initializes the object by calling the InitializeDevice
            % function
            obj = InitializeDevice(obj);
        end
        
        
        function [varargout] = goSafe2Temp(obj,Value,varargin)
            if obj.Output == false
                error('Output turned of. To go safe to current, turn output on')
            end
            
            if Value > obj.MaxTemp
                error([obj.Name ':' 'goSafe2Temp - Final temperature value violates maximum temperature!'])
            elseif Value < obj.MinTemp
                error([obj.Name ':' 'goSafe2Temp - Final temperature value violates minimum temperature!'])
            end
            
            Difference = Value - obj.ActTemp;
            StartTemp = obj.ActTemp;
            %             EndCurrent = Value;
            StepSize = 0.5/60;
            
            
            Show = false;
            if isempty(varargin)
                Speed = 1*60;
            else
                Speed = varargin{1}*60;
                if length(varargin)>2
                    Show = varargin{2};
                end
            end
            
            
            
            
            if Speed <= 1*60
            elseif and(Speed > 1*60,Speed<=2*60);
                disp('goSafe2Temp increased Stepsize to .2K')
                StepSize = 1/60;
                Speed = Speed/2;
            elseif and(Speed > 2*60,Speed<=4*60);
                disp('goSafe2Temp increased Stepsize to .4K')
                StepSize = 2/60;
                Speed = Speed/4;
            elseif and(Speed > 4*60,Speed<=8*60);
                disp('goSafe2Temp increased Stepsize to .8K')
                StepSize = 4/60;
                Speed = Speed/8;
            else
                error('goSafe2Temp cant go faster than 8K per Minute due to safety reasons')
            end
            
            
            StepNumber = Difference / StepSize;
            StepNumber = floor(StepNumber);
            
            
            
            if ~(StepNumber == 0)
                disp([obj.Name ': ' 'goSafe2Temp from:' ' ' num2str(round(obj.ActTemp,2)) 'C to' ' ' num2str(round(Value,2)) 'C'])
                f = waitbar(0,[obj.Name ': ' 'Go to Temp ' num2str(round(Value,2)) 'C: ' num2str(round(obj.ActTemp,2)) 'C']);
            end
            
            if StepNumber < 0;
                StepSize = -StepSize;
                StepNumber = -StepNumber;
            elseif StepNumber == 0
                
                disp('goSafe2Current failed: Set Current = Act Current')
            end
            
            OutCurrent = [];
            OutVoltage = [];
            OutSetTemp = [];
            OutActTemp = [];
            TimeArray = [];
            
            
            TimeStart = tic;
            for i=1:floor(StepNumber)
                tic
                waitbar(i/(floor(StepNumber)+1),f,[obj.Name ': ' 'Go to Temp ' num2str(round(Value,2)) 'C: ' num2str(round(obj.ActTemp,2)) 'C']);
                
                TimeStart2 = tic;
                obj.SetTemp = StartTemp + i*StepSize;
                if Show
                    disp([num2str(obj.ActTemp) ' ' 'mA'])
                end
                
                OutCurrent(i) = obj.PeltCurrent;
                OutVoltage(i) = obj.Voltage;
                OutSetTemp(i) = obj.SetTemp;
                OutActTemp(i) = obj.ActTemp;
                TimeArray(i) = toc(TimeStart);
                TimeArray(i);
                %                 plot(TimeArray,OutActTemp)
                WaitTime = toc(TimeStart2);
                %                 1/(Speed/StepNumber)*abs(StepSize)*60
                pause(1/(Speed/StepNumber)*abs(StepSize)*60-WaitTime)
            end
            
            obj.SetTemp = Value;
            if ~(StepNumber ==0)
                waitbar(1,f,[obj.Name ': ' 'Go to Temp ' num2str(round(Value,2)) 'C: ' num2str(round(obj.ActTemp,2)) 'C']);
                TimeArray = TimeArray - TimeArray(1);
                pause(.5)
                delete(f)
            end
            if Show
                disp([num2str(obj.ActTemp) 'C'])
            end
            varargout{1} = [TimeArray' OutSetTemp' OutActTemp' OutCurrent' OutVoltage'];
            
            
            
        end
        
        function switchOff(obj)
            if obj.Output == true
                obj.goSafe2Temp(23.5);
                disp([obj.Name ' ' 'switches off in 2s'])
                pause(2)
                obj.Output = false;
                disp([obj.Name ' ' 'successfully switched off'])
            else
                disp([obj.Name ' ' 'already switched off'])
            end
        end
        
    end
    
    
    
    
    
    
    
    
    
    %% GET methods
    methods
        
        
        function Answer = get.Voltage(obj)
            % Visa Read Voltage
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':VTE:ACT?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        function Answer = get.PeltCurrent(obj)
            % Visa Read actual current
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':ITE:ACT?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        function Answer = get.SLimCurrent(obj)
            % Visa Read software limit of current
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':LIMT:SET?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        function Answer = get.HLimCurrent(obj)
            % Visa Read hardware limit of current
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':LIMTP:ACT?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        function Answer = get.Output(obj)
            % Visa Read if power output is on or off
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':TEC?'); % Communication with device
            
            % Convert answer to boolean
            if strcmp(DeviceAnswer(1:3),'OFF')
                Answer = false;
            elseif strcmp(DeviceAnswer(1:2),'ON')
                Answer = true;
            else
                error('Output Readout ERROR')
            end
            
        end
        
        
        function Answer = get.SetTemp(obj)
            % Visa Read the chosen current
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':TEMP:SET?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        function Answer = get.ActTemp(obj)
            % Visa Read the chosen current
            obj.SlotIni; % needed to choose correct slot
            [DeviceAnswerRaw,DeviceAnswer] = VisaRead(obj,':TEMP:ACT?'); % Communication with device
            Answer = str2double(DeviceAnswer);
        end
        
        
        
    end
    
    
    
    
    
    
    
    %% SET methods
    methods
        
        
        function obj = set.Slot(obj,Value)
            % Set the slot
%             addpath('../../../Functions')
            
            % Check input
            Flag1 = and(isfloat(Value),Value == floor(Value)); % It must be an integer
            Flag2 = Value <= 8 & Value >= 1; % It must be between 1 and 8
            if and(Flag1,Flag2)
                obj.Slot = Value; % Set the value
            else
                error('Slot must be integer between 1 and 8 ERROR#1')
            end
            
        end
        
        
        
        function obj = set.SetTemp(obj,Value)
            % Set the desired Current of the device
            obj.SlotIni; % needed to choose correct slot
            
            % Check input
            if ~isfloat(Value) % input must be a float
                error('Input must be a number')
            end
            
            % Check so that Max and Min limit are not exceeded
            if isempty(obj.MinTemp)
                error('Before Setting the Temperature a minimum must be entered')
            elseif isempty(obj.MaxTemp)
                error('Before Setting the Temperature a maximum must be entered')
            end
            
            if ~and(Value >= obj.MinTemp,Value <= obj.MaxTemp)
                error('Set Value violates Max and Min Temp')
            end
            
            % Communication with device
            StrValue = num2str(Value,'%e'); % Convert input to float number with exponential and that to string
            PreCommand = ':TEMP:SET';
            Command = [PreCommand ' ' StrValue];
            VisaWrite(obj,Command);
        end
        
        
        
        
        function obj = set.Output(obj,Value)
            % Enables or disables the output
            obj.SlotIni; % needed to choose correct slot
            
            
            % Check input and convert boolean to Strings
            if and(Value == true,obj.Output == true) % check if already turned on - create warning
                disp([obj.Name ' ' 'already turned on'])
                StrValue = 'ALREADYON';
            elseif and(Value == false,obj.Output == false)
                disp([obj.Name ' ' 'already turned off - however communicating']) % if already
                StrValue = 'OFF';
            elseif Value == true;
                
                StrValue = 'ON';
                if and(Value,~(obj.ActTemp == obj.SetTemp));
                    disp([obj.Name ':' 'For turning on the TEC the Temperature is set to actual Temperature'])
                    obj.SetTemp = obj.ActTemp;
                end
            elseif Value == false
                StrValue = 'OFF';
                
                if ~isempty(obj.ConnectedLDC)
                    if obj.ConnectedLDC.Output
                        error('Must switch off LDC before turning off')
                    end
                    obj.SlotIni;
                end
                
            else
                error('Value must be Boolean')
            end
            
            
            if ~strcmp(StrValue,'ALREADYON') % if device is already turned on - cancel communication to save time
                % Communication with device
                if Value == true
                    warning([obj.Name ':' 'Switching on requires waiting of 2 seconds - Safety reason. '])
                    pause(2)
                end
                
                if Value == false
                    warning([obj.Name ':' 'Switching of requires waiting of 20 seconds - Safety reason. '])
                    pause(20)
                end
                PreCommand = ':TEC';
                Command = [PreCommand ' ' StrValue];
                VisaWrite(obj,Command);
                pause(.5)
                
                % Read out the device to check if it worked. If device is not
                % interlocked output will not turn on and a error message will
                % appear
            end
            if and(obj.Output == false,Value == true)
                error('Check Interlock')
            end
        end
        
        
        
        
        
        % Due to security reasons some Values are not allowed to be written to the
        % device (e.g. software limit). Some values also can't be written, because the device
        % produces them itself (e.g. Voltage).
        function obj = set.PeltCurrent(obj,Value)
            error('Can only be read. To change current, change SetCurrent!')
        end
        
        function obj = set.HLimCurrent(obj,Value)
            error('Can only be read due to safety reasons - Change at device!')
        end
        
        function obj = set.SLimCurrent(obj,Value)
            error('Can only be read due to safety reasons - Change at device!')
        end
        
        function obj = set.Voltage(obj,Value)
            error('Can only be read. Device property.')
        end
        
        function obj = set.ActTemp(obj,Value)
            error('Can only be read. Device property.')
        end
        
        function obj = set.MinTemp(obj,Value)
            if ~isfloat(Value)
                error('Value must be number.')
            end
            if ~isempty(obj.MaxTemp)
                if Value >= obj.MaxTemp
                    error('Value must be smaller than maximal temperature')
                end
            end
            obj.MinTemp = Value;
        end
        
        function obj = set.MaxTemp(obj,Value)
            if ~isfloat(Value)
                error('Value must be number.')
            end
            
            if ~isempty(obj.MinTemp)
                if Value <= obj.MinTemp
                    error('Value must be higher than maximal temperature')
                end
            end
            
            obj.MaxTemp = Value;
        end
        

        
        
        
        
        
        
    end
    
end


