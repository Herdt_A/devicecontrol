%% Header % Comment if you use in a larger script

instrreset
clear variables
close all
clc

%% ################### Initialization



LDC =  Dev.ThorProLDC;
LDC.VisaAdress = 'GPIB0::9::INSTR'; %% TODO: Find GPIB Adress
LDC.VisaType = 'visa-gpib';
LDC = LDC.DevIni;
LDC.Name = 'LDC Thorlabs800 Matteo';
LDC.Slot = 2;



% M1 = motor;
% M1Adress = '59937830';
% M1.connect(M1Adress);

%Create Bruker Resolution = 0.075, Number of Scans = 10, High Freq Lim =
%Max Low Freq Lim = Min
Bruker = Dev.Vertex(0.1,20, 15000,10);
Bruker = Bruker.setDetector_InGaAs_AC();


directory1 = 'C:\Users\HLO\Desktop\Matteo\20190411_LaserC150\';
% Bruker.AQM = 'SN';

%% ################## SET DEVICE PARAMETERS ##################

%% TODO: Software Limit in Amps

LDC.LaserObject.Current_Lim = 1.000;
LDC.SLimCurrent = .600;
% LDC.switchOff();
% Set Current Stepsizes
StartCurrent =    0.180;
EndCurrent   =    0.210;
StepSizeCurrent = 0.010;


%% ################## Read Out





mkdir(directory1)
% ################## 01. ### Switch on Laser / set it above threshold

% LDC.Output = true;
LDC.goSafe2Current(StartCurrent,8);


NumCurrentSteps = round((EndCurrent - StartCurrent)/StepSizeCurrent + 1);


% ################## 02. ### Gather ESA Signal for each Frequency and

    for i=1:NumCurrentSteps
        % ################## 02.a) # Go To Current
        
        
        NextCurrent =StartCurrent+StepSizeCurrent*(i-1);
        CurrentList(i) = NextCurrent;
        
        disp(['Next Current =' ' ' num2str(NextCurrent*1000) 'mA'])
        
        LDC.goSafe2Current(NextCurrent);
        pause(0.2)
        CurrentActList(i) = LDC.ActCurrent;
        
        FileNameNew = [num2str(NextCurrent*1000) 'mA.Opus'];
        disp(['Start Vertex Measurement for' ' ' num2str(NextCurrent*1000) ' ' 'mA'])
        Bruker.startMeasurement([directory1 'Messung' num2str(i) ]);
        clc
        disp(['Finished Measurement and Saving Data for' ' ' num2str(NextCurrent*1000) 'mA'])
        
        BrukerFiles = dir([directory1 '*.0']);
        BrukerFileName = BrukerFiles(end).name;
        movefile([directory1 BrukerFileName],[directory1 FileNameNew],'f');
        interferogram = Dev.Util.OpusReadIn([directory1 FileNameNew]);
        figure(1)
                plot(interferogram(:,1),interferogram(:,2))
        xlabel('distance [m]')
        ylabel('intensity')
        Dev.Util.SafeToAsci_extern(interferogram, directory1, [FileNameNew(1:end-5) 'ifg'])
        spectrum = Dev.Util.IFG2Spec([directory1 FileNameNew],1100,1800);
        figure(2)
        plot(spectrum(:,1),spectrum(:,2))
        xlabel('wavelength [nm]')
        ylabel('intensity')
        
        
        Dev.Util.SafeToAsci_extern(spectrum, directory1, [FileNameNew(1:end-5) 'spec'])
        clc
        disp(['Saving successful for' ' ' num2str(NextCurrent*1000) 'mA'])
    end

LDC.switchOff;
