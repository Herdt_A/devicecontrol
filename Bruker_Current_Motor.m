%% Header % Comment if you use in a larger script

instrreset
clear variables
close all
clc

%% ################### Initialization



LDC =  Dev.ILX_LDX3210;
LDC.VisaAdress = 'GPIB0::2::INSTR';
LDC = LDC.DevIni;
LDC.Name = 'LDC ICL2183_13_25';




m1 = Dev.K10CR1();
M1Adress = '59937830';
M1.connect(M1Adress);

%Create Bruker Resolution = 0.075, Number of Scans = 10, High Freq Lim =
%Max Low Freq Lim = Min
Bruker = Dev.Vertex(0.075,5, 15790.460824,10);


%% ################## SET DEVICE PARAMETERS ##################

% ################## LASER
Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;




% ################## LASERLIMITS
% Set Laserlimits and so on
LDC.LaserObject = Laser_ICL2183_10_12;
LDC.SLimCurrent = .05;
SafeCurrent = 0.002;

% Set Current Stepsizes
StartCurrent =    0.020;
EndCurrent   =    0.030;
StepSizeCurrent = 0.001;


% Set Motor Stepsizes
StartAngle      = 0;
EndAngle        = 90;
StepSizeAngle   = 1;

%% ################## Read Out




directory1 = 'C:\Users\HLO\Nextcloud\Measurement\Andreas\20190410_Vertex\';

% ################## 01. ### Switch on Laser / set it above threshold

LDC.Output = true;
LDC.goSafe2Current(StartCurrent);


NumCurrentSteps = round((EndCurrent - StartCurrent)/StepSizeCurrent + 1);
NumMotorSteps = round((EndAngle - StartAngle)/StepSizeAngle + 1);


% ################## 02. ### Gather ESA Signal for each Frequency and
% current using the lock in

for ii =1:NumMotorSteps
    
    NextAngle =StartAngle+StepSizeAngle*(ii-1);
    AngleList(ii) = NextAngle;
    m1.moveto(NextAngle);
        
    for i=1:NumCurrentSteps;
        % ################## 02.a) # Go To Current
        
        
        NextCurrent =StartCurrent+StepSizeCurrent*(i-1);
%         CurrentList(i,ii) = NextCurrent;
        
        disp(['Next Current =' ' ' num2str(NextCurrent*1000) 'mA'])
        
        LDC.goSafe2Current(NextCurrent);
        pause(0.2)
%         CurrentActList(i) = LDC.ActCurrent;
        
        FileNameNew = [num2str(NextAngle) 'Deg_' num2str(NextCurrent*1000) 'mA.Opus'];
        Bruker.startMeasurment([directory1 'Messung' i ]);

        BrukerFiles = dir([directory1 '*.0']);
        BrukerFileName = BrukerFiles(end).name;
        movefile([directory1 BrukerFileName],[directory1 FileNameNew],'f');
        interferogram = Dev.Util.OpusReadIn([directory1 FileNameNew]);
        Dev.Util.SafeToAsci_extern(interferogram, directory1, [FileNameNew(1:end-5) 'ifg'])
        spectrum = Dev.Util.IFG2Spec([directory1 FileNameNew],3000,3400);
        Dev.Util.SafeToAsci_extern(spectrum, directory1, [FileNameNew(1:end-5) 'spec'])        
    end

end
LDC.switchOff;
