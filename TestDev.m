% % % clear variables
% % % close all
% % % clc
% % % 
% % % 
% % % 
% % % % % ################### TEST ThorTec
% % % % B =  Dev.Comm.VisaObject;
% % % % B =  Dev.ThorProTEC;
% % % % B.VisaAdress = 'GPIB0::1::INSTR';
% % % % B.VisaType = 'visa-gpib';
% % % % B.Slot =2;
% % % % B.LaserObject.Temp_Min = 20;
% % % % B.Name = 'TEC 1';
% % % % 
% % % % B = B.DevIni;
% % % % % A.Output = 1;
% % % % 
% % % % % A.SlotIni;
% % % % % tic
% % % % % tic
% % % % % A.goSafe2Temp(21.5,2);
% % % % % toc
% % % % 
% % % % B.SetTemp = 21.5;
% % % % 
% % % % 
% % % % % pause(2)
% % % % % ################### TEST ThorLDC
% % % % A =  Dev.ThorProLDC;
% % % % 
% % % % A.VisaAdress = 'GPIB0::1::INSTR';
% % % % A.VisaType = 'visa-gpib';
% % % % % A.Slot =1;
% % % % % A.LaserObject.Temp_Min = 20;
% % % % A.LaserObject.Current_Lim = 0.14;
% % % % A.Name = 'LDC 1';
% % % % 
% % % % 
% % % % A.Slot = 1;
% % % % A = A.DevIni;
% % % % 
% % % % A.Output = false;
% % % % A.SetCurrent = 0.01;
% % % % A.Output = true;
% % % % A.SetCurrent = 0.01;
% % % % % A = A.Create;
% % % % 
% % % % A.SLimCurrent = .08;
% % % % 
% % % % 
% % % % % ################### TEST Osci DPO3032
% % % % A =  Dev.DPO3032;
% % % % A.VisaAdress = 'USB0::0x0699::0x0412::C010898::0::INSTR';
% % % % A.VisaType = 'visa-usb';
% % % % A = A.DevIni;
% % % % 
% % % % A = A.getTrace(1,100000);
% % % % plot(A.Trace(:,1),A.Trace(:,2))
% % % 
% % % 
% % % % % ################### TEST ESA FSC 6
% % % 
% % % % A = Dev.RS_FSC6;
% % % % A.VisaAdress = 'TCPIP0::130.83.3.194::inst0::INSTR';
% % % % A = A.DevIni;
% % % % 
% % % % 
% % % % 
% % % % figure
% % % % xlabel('Freq. [MHz]')
% % % % ylabel('spec. power density [dB]')
% % % % set(gca,'FontSize',14)
% % % % 
% % % % 
% % % % hold on
% % % % 
% % % % 
% % % % List(1) = 1e6;
% % % % List(2) = 2e7;
% % % % List(3) = 3e8;
% % % % List(4) = 4e8;
% % % % List(5) = 5e8;
% % % % List(6) = 6e8;
% % % % List(7) = 7e8;
% % % % List(8) = 8e8;
% % % % List(9) = 9e8;
% % % % List(10) = 10e8;
% % % % 
% % % % 
% % % % for i=1:10
% % % % A.FreqStop = List(i);
% % % % A = A.getTrace;
% % % % plot(A.Trace(:,1),A.Trace(:,2))
% % % % drawnow
% % % % Dings(i) = A.FreqStop;
% % % % end
% % % % 
% % % % hold off
% % % % plot(Dings(i));
% % % % hold on
% % % % for i=1:100
% % % % A = A.getTrace;
% % % % plot(A.Trace(:,1)/1e6,A.Trace(:,2))
% % % % drawnow
% % % % end
% % % % hold off
% % % 
% % % % % ################### TEST Agilent
% % % 
% % % A = Dev.Agilent34401A;
% % % A.VisaAdress = 'GPIB0::23::INSTR';
% % % A = A.DevIni;
% % % 
% % instrreset
% % % A = Dev.HP8116A;
% % % A.VisaAdress = 'GPIB0::16::INSTR';
% % % A = A.DevIni;
% % % 
% % % A.Frequency = 2e6;
% % % A.Frequency
% % 
% % % A = Dev.Keithley2400('GPIB0::24::INSTR');
% % % A.VisaAdress = 'GPIB0::24::INSTR';
% % % A.VisaType = 'visa-gpib';
% % % A = A.DevIni;
A = Dev.SR530LockIn;
A.VisaAdress = 'GPIB0::23::INSTR';
A = A.DevIni;
A.AutoInc = false;
i=0;
while true
    i=i+1
    X(i) = A.OutputAmp;
    plot(X,'-x');
    pause(A.TimeAverage)
end
% % 
% instrreset
% % clc
% clear all
% LDC =  Dev.ILX_LDX3210;
% LDC.VisaAdress = 'GPIB0::2::INSTR';
% LDC = LDC.DevIni;
% LDC.Name = 'LDC ICL2183_13_25';
% Laser_ICL2183_10_12 = Dev.Lasers.ICL2183_10_12;
% 
% 
% 
% 
% % ################## LASERLIMITS
% % Set Laserlimits and so on
% LDC.LaserObject = Laser_ICL2183_10_12;
% LDC.SLimCurrent = .05;
% 
% LDC.switchOff;