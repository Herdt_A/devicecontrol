%% HEADER

clc
clear variables
close all

%% PROPERTIES
DefaultPath = 'E:\Nextcloud\Measurement\Olivier';




%% 
selpath = uigetdir(DefaultPath);

Files = dir([selpath '\*.oscitrc']);



for i=1:length(Files)
    
    Data2Save = Dev.Util.LoadOsciTrace(selpath,Files(i).name);
    NewFileName = Files(i).name;
    NewFileName = NewFileName(1:end-8);
    
    FullFileName = [selpath '\' NewFileName '.csv'];
    
    Dev.Util.SafeToAsci_extern(Data2Save,selpath,NewFileName,',');
%     clc
    Dev.Util.cprintf('*[1,0.5,0]',['Processing File' ' ' num2str(i) ' ' 'of' ' ' num2str(length(Files)) '\n'])
    Dev.Util.cprintf('*[1,0.5,0]',['Converted to:' ' ' strrep(FullFileName,'\','/') '\n'])
end

