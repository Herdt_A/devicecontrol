clear all
close all
addpath('DPO3032')
addpath('ILX')

Osci1 = Osci_DPO3032;
Osci1 = Osci1.Initialize;

LDC1 = LDX3232;
LDC1.VisaAdress = 'GPIB0::1::INSTR';
LDC1 = LDC1.Initialize;


LDC1.goSafe2Current(0.5);
% 
% 
% LDC1.Output = true;
% 
% 
for i = 500:850
LDC1.SetCurrent = i/1000;

i;
disp(['Run' num2str(i) ': at ' num2str(LDC1.ActCurrent*1000),'mA'])

Answer = Osci1.getTrace(1,1000000,false);
p1 = plot(Answer(:,1),Answer(:,2),'-','LineSmoothing','on','LineWidth',0.1);
p1.Color(4)=0.25;
filename = dl(num2str(i),'mA.mat');
dlmwrite(filename,Answer);
pause(5)

end
